//
//  LanguageAlertViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 12.01.2022.
//

import UIKit

class LanguageAlertController: UIViewController {
    
    let LANGUAGE_KEY = "CurrentLanguage"
    var defaults = UserDefaults.standard
    weak var delegate : SignInViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    @IBAction func setTurkmenAction(_ sender: Any) {
        defaults.set("tk", forKey: LANGUAGE_KEY)
        NotificationCenter.default.post(name: AppNotification.changeLanguage, object: nil)
        delegate?.changeLanguage()
        self.dismiss(animated: true, completion: nil)
 
    }
    @IBAction func setRussanAction(_ sender: Any) {
        defaults.set("ru", forKey: LANGUAGE_KEY)
        NotificationCenter.default.post(name: AppNotification.changeLanguage, object: nil)
        delegate?.changeLanguage()
        self.dismiss(animated: true, completion: nil)
 
    }
    @IBAction func setEnglishButton(_ sender: Any) {
        defaults.set("en", forKey: LANGUAGE_KEY)
        NotificationCenter.default.post(name: AppNotification.changeLanguage, object: nil)
        delegate?.changeLanguage()
        self.dismiss(animated: true, completion: nil)
 
    }
    
}

//
//  AppLanguageAlertController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 19.01.2022.
//

import UIKit

class AppLanguageAlertController: UIViewController {

    let LANGUAGE_KEY = "CurrentLanguage"
    let defaults = UserDefaults.standard
    var gesture = UILongPressGestureRecognizer()
    var tmLabelGesture = UITapGestureRecognizer()
    var ruLabelGesture = UITapGestureRecognizer()
    var enLabelGesture = UITapGestureRecognizer()
    weak var delegate : SettingsViewControllerDelegate?
    
    @IBOutlet weak var deleteLabel: LocalizableLabel!
    @IBOutlet weak var tmLabel: UILabel!
    @IBOutlet weak var ruLabel: UILabel!
    @IBOutlet weak var enLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizingUI()
        
        gesture = UILongPressGestureRecognizer(target: self, action: #selector(deleteAction(gesture:)))
        gesture.minimumPressDuration = 0
        deleteLabel.addGestureRecognizer(gesture)
        deleteLabel.isUserInteractionEnabled = true
        deleteLabel.layer.cornerRadius = 5
        deleteLabel.layer.masksToBounds = true
        
        tmLabelGesture = UITapGestureRecognizer(target: self, action: #selector(setTurkmenAction(gesture:)))
        ruLabelGesture = UITapGestureRecognizer(target: self, action: #selector(setRussianAction(gesture:)))
        enLabelGesture = UITapGestureRecognizer(target: self, action: #selector(setEnglishAction(gesture:)))
        tmLabel.addGestureRecognizer(tmLabelGesture)
        ruLabel.addGestureRecognizer(ruLabelGesture)
        enLabel.addGestureRecognizer(enLabelGesture)
        tmLabel.isUserInteractionEnabled = true
        ruLabel.isUserInteractionEnabled = true
        enLabel.isUserInteractionEnabled = true
        
    
        
    }
    
    @objc func deleteAction(gesture : UILongPressGestureRecognizer) {
        if gesture.state == .began {
            deleteLabel.backgroundColor = Theme.buttonTapColor
            
        } else if gesture.state == .ended || gesture.state == .cancelled {
            deleteLabel.backgroundColor = Theme.backgroundColorLight
            self.dismiss(animated: true, completion: nil)

        }
        
    }
    
    @objc func setTurkmenAction(gesture : UITapGestureRecognizer) {
        defaults.set("tk", forKey: LANGUAGE_KEY)
        NotificationCenter.default.post(name: AppNotification.changeLanguage, object: nil)
        delegate?.changeLanguage()
        delegate?.setKey()
        self.dismiss(animated: true, completion: nil)
    }
    @objc func setRussianAction(gesture : UITapGestureRecognizer) {
        defaults.set("ru", forKey: LANGUAGE_KEY)
        NotificationCenter.default.post(name: AppNotification.changeLanguage, object: nil)
        delegate?.changeLanguage()
        delegate?.setKey()
        self.dismiss(animated: true, completion: nil)
    }
    @objc func setEnglishAction(gesture : UITapGestureRecognizer) {
        defaults.set("en", forKey: LANGUAGE_KEY)
        NotificationCenter.default.post(name: AppNotification.changeLanguage, object: nil)
        delegate?.changeLanguage()
        delegate?.setKey()
        self.dismiss(animated: true, completion: nil)
    }
    
    private func customizingUI() {
        self.deleteLabel.textColor = UIColor(cgColor: CGColor(red: CGFloat(0x3E)/255, green: CGFloat(0xA3)/255, blue: CGFloat(0xEB)/255, alpha: 1))
        self.view.backgroundColor = Theme.backgroundColorLight
        self.tmLabel.textColor = Theme.textColor
        self.ruLabel.textColor = Theme.textColor
        self.enLabel.textColor = Theme.textColor
    }
    
}

//
//  DeleteHistoryViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 01.02.2022.
//

import UIKit

class RemoveHistoryAlertController: UIViewController {

    var messageString : String? = nil
    var removeLongPress = UILongPressGestureRecognizer()
    var cancelLongPress = UILongPressGestureRecognizer()
 
    weak var delegate : MyCardsViewControllerDelegate?
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var cancelLabel: LocalizableLabel!
    @IBOutlet weak var removeLabel: LocalizableLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizingUI()

        if messageString != nil {
            messageLabel.text = messageString
        }
        
        //MARK: cancelLabel gestureRecognizer
        cancelLongPress = UILongPressGestureRecognizer(target:self, action: #selector(cancelAction(gesture:)))
        cancelLongPress.minimumPressDuration = 0
        cancelLabel.isUserInteractionEnabled = true
        cancelLabel.addGestureRecognizer(cancelLongPress)
        cancelLabel.layer.cornerRadius = 5
        cancelLabel.layer.masksToBounds = true

        //MARK: removeLabel gestureRecognizer
        removeLongPress = UILongPressGestureRecognizer(target:self, action: #selector(removeAction(gesture:)))
        removeLongPress.minimumPressDuration = 0
        removeLabel.isUserInteractionEnabled = true
        removeLabel.addGestureRecognizer(removeLongPress)
        removeLabel.layer.cornerRadius = 5
        removeLabel.layer.masksToBounds = true

  
        
    }
    
    @objc func removeAction(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            removeLabel.backgroundColor = Theme.buttonTapColor
            
        } else if gesture.state == .ended || gesture.state == .cancelled {
            removeLabel.backgroundColor = Theme.backgroundColorLight
            delegate?.deleteCardToIndex()
            delegate?.reloadDataTableView()
            self.dismiss(animated: true, completion: nil)
      
        }
    }
    @objc func cancelAction(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            cancelLabel.backgroundColor = Theme.buttonTapColor
            
        } else if gesture.state == .ended || gesture.state == .cancelled {
            cancelLabel.backgroundColor = Theme.backgroundColorLight
            self.dismiss(animated: true, completion: nil)
      
        }
      }
    
    private func customizingUI() {
        self.view.backgroundColor  = Theme.backgroundColorLight
        self.cancelLabel.textColor = UIColor(cgColor: CGColor(red: CGFloat(0x3E)/255, green: CGFloat(0xA3)/255, blue: CGFloat(0xEB)/255, alpha: 1))
        self.removeLabel.textColor = UIColor(cgColor: CGColor(red: CGFloat(0x3E)/255, green: CGFloat(0xA3)/255, blue: CGFloat(0xEB)/255, alpha: 1))
    }
}

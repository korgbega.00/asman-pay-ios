//
//  TermsUseAlertController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 12.01.2022.
//

import UIKit

class TermsUseAlertController: UIViewController {

    var acceptGesture = UILongPressGestureRecognizer()
    var notAcceptGesture = UILongPressGestureRecognizer()
    weak var delegate : SignInViewControllerDelegate?

    
    @IBOutlet weak var acceptLabel: UILabel!
    @IBOutlet weak var notAccept: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        acceptGesture = UILongPressGestureRecognizer(target: self, action: #selector(acceptAction(gesture:)))
        acceptGesture.minimumPressDuration = 0
        acceptLabel.addGestureRecognizer(acceptGesture)
        acceptLabel.isUserInteractionEnabled = true
        acceptLabel.layer.cornerRadius = 5
        acceptLabel.layer.masksToBounds = true
   
        notAcceptGesture = UILongPressGestureRecognizer(target: self, action: #selector(notAcceptAction(gesture:)))
        notAcceptGesture.minimumPressDuration = 0
        notAccept.addGestureRecognizer(notAcceptGesture)
        notAccept.isUserInteractionEnabled = true
        notAccept.layer.cornerRadius = 5
        notAccept.layer.masksToBounds = true
    

    }
    
    @objc func acceptAction(gesture: UILongPressGestureRecognizer) {
        delegate?.termsOfUse(true)
        self.dismiss(animated: true, completion: nil)
        
    }
    @objc func notAcceptAction(gesture: UILongPressGestureRecognizer) {
        delegate?.termsOfUse(false)
        self.dismiss(animated: true, completion: nil)
  
    }
    
}

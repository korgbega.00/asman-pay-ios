//
//  SetAppKeyAlertController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 19.01.2022.
//

import UIKit

class SetAppKeyAlertController: UIViewController {

    var appCode : String?
    var deleteTouchDown = UILongPressGestureRecognizer()
    var pastTouchDown = UILongPressGestureRecognizer()
    
    let LANGUAGE_KEY = "CurrentLanguage"
    let defaults = UserDefaults.standard
    var currentLanguage = "ru"
    
    weak var delegate : SettingsViewControllerDelegate?

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var textFieldLabel: UILabel!
    @IBOutlet weak var texCountLabel: UILabel!
    @IBOutlet weak var deleteLabel: UILabel!
    @IBOutlet weak var pastLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customizingUI()
        
        appCode = UserDefaults.standard.string(forKey: "appCode")
        if let language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        textField.delegate = self
//        textField.keyboardType = .numberPad
        
        //MARK: customize view
        textField.layer.cornerRadius = 5
        textField.layer.borderWidth = 1
        textField.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
        textField.tintColor = .red
        deleteLabel.layer.cornerRadius = 5
        deleteLabel.layer.masksToBounds = true
        pastLabel.layer.cornerRadius = 5
        pastLabel.layer.masksToBounds = true
        
        //MARK: deleteLabel gestureRecognizer
        deleteTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(deleteAction(gesture:)))
        deleteTouchDown.minimumPressDuration = 0
        deleteLabel.isUserInteractionEnabled = true
        deleteLabel.addGestureRecognizer(deleteTouchDown)

        //MARK: pastLabel gestureRecognizer
        pastTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(pastAction(gesture:)))
        pastTouchDown.minimumPressDuration = 0
        pastLabel.addGestureRecognizer(pastTouchDown)

        
        pastLabel.isUserInteractionEnabled = false
        pastLabel.alpha = 0.5
        
        if appCode == nil {
            if currentLanguage == "tk" {
                titleLabel.text = "Açar söz goý"
                pastLabel.text = "GOÝ"

            } else if currentLanguage == "en" {
                titleLabel.text = "Set passcode"
                pastLabel.text = "SET"
                
            } else if currentLanguage == "ru" {
                titleLabel.text = "Установить код блокировки"
                pastLabel.text = "УСТАНОВИТЬ"
               
            }
        } else {
            if currentLanguage == "tk" {
                titleLabel.text = "Açar sözi poz"
                pastLabel.text = "POZ"
                
            } else if currentLanguage == "en" {
                titleLabel.text = "Remove passcode"
                pastLabel.text = "REMOVE"
                    
            } else if currentLanguage == "ru" {
                titleLabel.text = "Удалить код блокировки"
                pastLabel.text = "УДАЛИТЬ"
                   
            }
            
        }
           
    }
    
    @objc func pastAction(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            pastLabel.backgroundColor = Theme.buttonTapColor
            
        } else if gesture.state == .ended || gesture.state == .cancelled {
            pastLabel.backgroundColor = Theme.backgroundColorLight
            if appCode == nil {
                    UserDefaults.standard.set(textField.text, forKey: "appCode")
                
            } else if appCode == textField.text {
                UserDefaults.standard.removeObject(forKey: "appCode")
            }
            self.delegate?.setKey()
            self.dismiss(animated: true, completion: nil)

        }
    }
    
    @objc func deleteAction(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            deleteLabel.backgroundColor = Theme.buttonTapColor
                    
            } else if gesture.state == .ended || gesture.state == .cancelled {
                deleteLabel.backgroundColor = Theme.backgroundColorLight
                self.dismiss(animated: true, completion: nil)
            
            }
    }
    
    private func customizingUI() {
        self.view.backgroundColor = Theme.backgroundColorLight
        self.textFieldLabel.textColor = .systemRed
        self.texCountLabel.textColor = .systemRed
        self.textField.backgroundColor = Theme.backgroundColorLight
        self.pastLabel.textColor = UIColor(cgColor: CGColor(red: CGFloat(0x3E)/255, green: CGFloat(0xA3)/255, blue: CGFloat(0xEB)/255, alpha: 1))
        self.deleteLabel.textColor = UIColor(cgColor: CGColor(red: CGFloat(0x3E)/255, green: CGFloat(0xA3)/255, blue: CGFloat(0xEB)/255, alpha: 1))
        self.textField.textColor = Theme.textColor
    }
    
}
extension SetAppKeyAlertController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text {
            let invalidCharacters =
                CharacterSet ( charactersIn: "1234567890" ) .inverted
      
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            print(newString)
            
            if  newString.count < 4 {
                pastLabel.isUserInteractionEnabled = false
                pastLabel.alpha = 0.5
                if ( string.rangeOfCharacter (from: invalidCharacters) == nil ) {
                    texCountLabel.text = "\(newString.count)/4"
                }
            
                self.textField.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
                self.textField.tintColor = .red
                errorImageView.alpha = 1.0
                textFieldLabel.alpha = 1.0
                return ( string.rangeOfCharacter (from: invalidCharacters) == nil )
            }
            if  newString.count == 4 {
                pastLabel.isUserInteractionEnabled = true
                pastLabel.alpha = 1.0
                texCountLabel.text = "\(newString.count)/4"
                
                self.textField.layer.borderColor = CGColor(red: CGFloat(0xBE)/255, green: CGFloat(0xBE)/255, blue: CGFloat(0xBE)/255, alpha: 1)
                self.textField.tintColor = UIColor(red: CGFloat(0x3E)/255, green: CGFloat(0xA3)/255, blue: CGFloat(0xEB)/255, alpha: 1)
                self.texCountLabel.textColor = Theme.textColor
                errorImageView.alpha = 0.0
                textFieldLabel.alpha = 0.0
                return ( string.rangeOfCharacter (from: invalidCharacters) == nil )
                
            }
            
        }
        return false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.textField.resignFirstResponder()
    }
}
    

//
//  ChooseCardAlertController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 26.01.2022.
//

import UIKit

class ChooseCardAlertController: UIViewController {

    var cards : [Card] = []
    var tmCell: TMCellViewController?
    var telecomInternet: TelecomInternetViewController?
    var astuInternetViewController : ASTUInternetViewController?
    var astuPhoneViewController : ASTUPhoneViewController?
    var astuIptvViewController : ASTUIptvViewController?
    var deleteLabelGesture = UILongPressGestureRecognizer()
    var addCardLabelGesture = UILongPressGestureRecognizer()
  
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var heightContentView: NSLayoutConstraint!
    @IBOutlet weak var deleteLabel: LocalizableLabel!
    @IBOutlet weak var addCardLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let data = UserDefaults.standard.data(forKey: "cards") {
            do {
                let decoder = JSONDecoder()
                let card = try decoder.decode([Card].self, from: data)
                cards = card
                
            } catch {
                print(error)
            }
        }
        
        customizeUI()
        tableView.delegate = self
        tableView.dataSource = self
        
        if cards.count == 0 {
            heightContentView.constant = CGFloat(105 + 100)
            tableView.isScrollEnabled = false
        } else if cards.count <= 5 {
            heightContentView.constant = CGFloat(105 + 45 * cards.count)
            tableView.isScrollEnabled = false
        } else {
            heightContentView.constant = CGFloat(105 + 45 * 5)
            tableView.isScrollEnabled = true
            
        }

    }
    @objc func deleteAction(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            deleteLabel.backgroundColor = Theme.buttonTapColor
            
        } else if gesture.state == .ended || gesture.state == .cancelled {
            deleteLabel.backgroundColor = Theme.backgroundColorLight
            self.dismiss(animated: true, completion: nil)

        }
    }
    @objc func addCardAction(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            addCardLabel.backgroundColor = Theme.buttonTapColor
            
        } else if gesture.state == .ended || gesture.state == .cancelled {
            addCardLabel.backgroundColor = Theme.backgroundColorLight
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NewCardViewControllerID") as! NewCardViewController
            self.dismiss(animated: true) {
                self.tmCell?.present(vc, animated: true, completion: nil)
                self.telecomInternet?.present(vc, animated: true, completion: nil)
                self.astuInternetViewController?.present(vc, animated: true, completion: nil)
                self.astuPhoneViewController?.present(vc, animated: true, completion: nil)
                self.astuIptvViewController?.present(vc, animated: true, completion: nil)
            }

        }
            
    }
    
    private func customizeUI(){
        //MARK: deleteLabel gestureRecognizer
        deleteLabelGesture = UILongPressGestureRecognizer(target:self, action: #selector(deleteAction(gesture:)))
        deleteLabelGesture.minimumPressDuration = 0
        deleteLabel.isUserInteractionEnabled = true
        deleteLabel.addGestureRecognizer(deleteLabelGesture)
        deleteLabel.layer.masksToBounds = true
        deleteLabel.layer.cornerRadius = 5

        //MARK: pastLabel gestureRecognizer
        addCardLabelGesture = UILongPressGestureRecognizer(target:self, action: #selector(addCardAction(gesture:)))
        addCardLabelGesture.minimumPressDuration = 0
        addCardLabel.isUserInteractionEnabled = true
        addCardLabel.addGestureRecognizer(addCardLabelGesture)
        addCardLabel.layer.masksToBounds = true
        addCardLabel.layer.cornerRadius = 5
        
        //Theme
        self.view.backgroundColor = Theme.backgroundColorLight
        self.deleteLabel.textColor = UIColor(cgColor: CGColor(red: CGFloat(0x3E)/255, green: CGFloat(0xA3)/255, blue: CGFloat(0xEB)/255, alpha: 1))
        self.addCardLabel.textColor = UIColor(cgColor: CGColor(red: CGFloat(0x3E)/255, green: CGFloat(0xA3)/255, blue: CGFloat(0xEB)/255, alpha: 1))
        self.tableView.backgroundColor = Theme.backgroundColorLight
    }
}
extension ChooseCardAlertController: UITableViewDataSource,UITableViewDelegate {
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cards.count == 0 {
            return 1
        } else {
            return cards.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->  UITableViewCell {
        //selected color
        let backgroundView = UIView()
        backgroundView.backgroundColor = Theme.defaultSelectedColor

        if cards.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlankCardsAlertCellID", for: indexPath) as! BlankCardsAlertCell
            cell.selectedBackgroundView = backgroundView
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CardCellID", for: indexPath) as! CardCell
            cell.label.text = "****\(cards[indexPath.row].title) \(cards[indexPath.row].nameLastName)"
            cell.selectedBackgroundView = backgroundView
            
            return cell
            
        }
    }
    
    //MARK: UITableViewDelegate
    func tableView (_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if cards.count == 0 {
            return 100
        } else {
            return 45
        }
    }
    
    func tableView (_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow (at: indexPath, animated: true)
        if cards.count != 0 {
            //tmCell
            tmCell?.card = cards[indexPath.row]
            tmCell?.defaultSelectedView()
            tmCell?.selectedViewTitle.font = .boldSystemFont(ofSize: 15.0)
            tmCell?.selectedViewTitle.text = "****\(tmCell!.card!.title) \(tmCell!.card!.nameLastName)"

            //telecomInternet
            telecomInternet?.card = cards[indexPath.row]
            telecomInternet?.defaultSelectedView()
            telecomInternet?.selectedViewTitle.font = .boldSystemFont(ofSize: 15.0)
            telecomInternet?.selectedViewTitle.text = "****\(telecomInternet!.card!.title) \(telecomInternet!.card!.nameLastName)"
            
            //astuInternetViewController
            astuInternetViewController?.card = cards[indexPath.row]
            astuInternetViewController?.defaultSelectedView()
            astuInternetViewController?.selectedViewTitle.font = .boldSystemFont(ofSize: 15.0)
            astuInternetViewController?.selectedViewTitle.text = "****\(astuInternetViewController!.card!.title) \(astuInternetViewController!.card!.nameLastName)"
            
            //astuPhoneViewController
            astuPhoneViewController?.card = cards[indexPath.row]
            astuPhoneViewController?.defaultSelectedView()
            astuPhoneViewController?.selectedViewTitle.font = .boldSystemFont(ofSize: 15.0)
            astuPhoneViewController?.selectedViewTitle.text = "****\(astuPhoneViewController!.card!.title) \(astuPhoneViewController!.card!.nameLastName)"
            
            //astuIptvViewController
            astuIptvViewController?.card = cards[indexPath.row]
            astuIptvViewController?.defaultSelectedView()
            astuIptvViewController?.selectedViewTitle.font = .boldSystemFont(ofSize: 15.0)
            astuIptvViewController?.selectedViewTitle.text = "****\(astuIptvViewController!.card!.title) \(astuIptvViewController!.card!.nameLastName)"
          
            self.dismiss(animated: true, completion: nil)
        }
   }
}


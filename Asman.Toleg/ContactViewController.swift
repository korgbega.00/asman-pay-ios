//
//  ContactViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 20.01.2022.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class ContactViewController: UIViewController {

    var tableView = UITableView()
    let utils = Utils()
    var bool  = true
    var topicTk  = ["Soragym bar", "Pul kartymdan gitdi yöne telefona gelmedi", "Sms kod gelenok",
    "Yalñyslyk yüze çykyar", "Basga", "Teklip ya-da maslahat", "«Kart maglumatlaryñyz nädogry» diyyar",
    "«Kartyñyza sms hyzmaty birikdirilmedik» diyyär"]
    var topicEn  = ["I got a question", "Money is gone from card but was not received", "Sms code is not received",
    "Keep receiving error", "Other", "Propose or Suggest", "I get «card information wrong error»",
    "I get «sms service is not connected error»"]
    var topicRu  = ["У меня вопрос", "Деньги с карты снились но не пришли на телефон", "Hе приходит смс код",
    "Ошибка при плате", "Другое", "Советы и предложения", "Получаю ошибку «неверная информация о карте»",
    "Получаю ошибку «смс сервис не подключен к карте»"]
    var topic : [String] = []
    var topicToSend: String? = nil
    let defaults  = UserDefaults.standard
    var language = "ru"
    var gestureRegonizersSuperView = UITapGestureRecognizer()
    var gestureRegonizersMessage = UITapGestureRecognizer()
    weak var activeField : UITextField?
    
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var selectMessageView: UIView!
    @IBOutlet weak var selectMessageImage: UIImageView!
    @IBOutlet weak var selectMessageLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var youLetterTextField: MDCOutlinedTextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentViewSafeArea: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var selectMessageMinLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        youLetterTextField.delegate = self
        
        customizingUI()
        
        if let currentLanguage = defaults.string(forKey: "CurrentLanguage") {
            language = currentLanguage
        }
        if language == "tk" {
            topic = topicTk
        }
        if language == "en" {
            topic = topicEn
        }
        if language == "ru" {
            topic = topicRu
        }
        
        utils.roundCorners(view: toolbarView, radius: 20)
        contentView.layer.cornerRadius = 10
        continueButton.layer.cornerRadius = 20
        
        selectMessageView.layer.borderWidth = 1
        selectMessageView.layer.cornerRadius = 5
        defaultSelectedView()
        gestureRegonizersMessage = UITapGestureRecognizer(target: self, action: #selector(selectMessageTap(sender:)))
        selectMessageView.addGestureRecognizer(gestureRegonizersMessage)
        
//        gestureRegonizersSuperView = UITapGestureRecognizer(target: self, action: #selector(superViewTap(sender:)))
//        self.view.addGestureRecognizer(gestureRegonizersSuperView)
    
        customizingTableView()
        
        // TODO: @Copy-pasta
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
        
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
//        view.addGestureRecognizer(tap)
      
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
    
    @IBAction func continueAction(_ sender: Any) {
        utils.validateNumberRange(view: youLetterTextField, min: 2, max: 100)
        if topicToSend == nil {
            errorSelectedView()
        }
        
        if utils.validateNumberRange(view: youLetterTextField, min: 2, max: 100) && topicToSend != nil {
            //code
            
        } else {
            //code
        }
   
    }
    
    @objc func selectMessageTap(sender : Any) {
        if bool {
            defaultSelectedView()
            scrollView.addSubview(tableView)
            tableView.topAnchor.constraint(equalTo: selectMessageView.bottomAnchor).isActive = true
            tableView.heightAnchor.constraint(equalToConstant: CGFloat((topic.count-1)*50)).isActive = true
            tableView.leftAnchor.constraint(equalTo: youLetterTextField.leftAnchor).isActive = true
            tableView.rightAnchor.constraint(equalTo: youLetterTextField.rightAnchor).isActive = true
            tableView.backgroundColor = Theme.backgroundColorLight
            
            selectMessageImage.image = UIImage(named: "triangle-up")?.withRenderingMode(.alwaysTemplate)
            selectMessageImage.tintColor = Theme.textColor
          
            bool = false
        } else {
            tableView.removeFromSuperview()
            selectMessageImage.image = UIImage(named: "triangle-down_1")?.withRenderingMode(.alwaysTemplate)
            selectMessageImage.tintColor = Theme.textColor

            bool = true
        }
    }
    
    //keyboard
    @objc func keyboardDidShow(notification: Notification) {
           print("show keyboard")
           let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
           guard let activeField = activeField, var keyboardHeight = keyboardSize?.height else { return }
           
           let tempKeyboardAdditional : CGFloat = 0.0
           let tempMoveAdditional : CGFloat = 0.0
           
           keyboardHeight += tempKeyboardAdditional  // TODO: Temporary hack. Find real value
           let moveViewFromBottomSize = keyboardHeight + tempMoveAdditional
           
           
           let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: moveViewFromBottomSize, right: 0.0)
           scrollView.contentInset = contentInsets
           scrollView.scrollIndicatorInsets = contentInsets
           let activeRect = activeField.convert(activeField.bounds, to: scrollView)
           scrollView.scrollRectToVisible(activeRect, animated: true)
       }

       @objc func keyboardWillBeHidden(notification: Notification) {
           print("Hide keyboard")
           let contentInsets = UIEdgeInsets.zero
           scrollView.contentInset = contentInsets
           scrollView.scrollIndicatorInsets = contentInsets
       }
       
       @objc func DismissKeyboard(){
           view.endEditing(true)
       }
    
    private func customizingTableView(){
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.layer.cornerRadius = 5
        tableView.sectionIndexColor = .black
        tableView.sectionIndexTrackingBackgroundColor = .red
        tableView.sectionIndexBackgroundColor = .red
        tableView.layer.borderWidth = 1
        tableView.layer.borderColor = UIColor.systemGray5.cgColor
        tableView.separatorStyle = .none
        tableView.backgroundColor = Theme.backgroundColorLight
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    public func errorSelectedView(){
        //red
        selectMessageView.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
        selectMessageMinLabel.textColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
        selectMessageMinLabel.alpha = 1
    }
    
    public func defaultSelectedView(){
        //C8CACB
        selectMessageView.layer.borderColor = CGColor(red: CGFloat(0xC8)/255, green: (0xCA)/255, blue: (0xCB)/255, alpha: 1.0)
        selectMessageMinLabel.alpha = 0
    }
    
    private func customizingUI() {
        if utils.getLanguage() == "tk" {
            youLetterTextField.label.text = "Hatyňyz"
            youLetterTextField.leadingAssistiveLabel.text = "Hatyňyzy giriziň"
            selectMessageMinLabel.text = "Hatyň mowzuguny saýlaň"

            
        } else if utils.getLanguage() == "en" {
            youLetterTextField.label.text = "Your message"
            youLetterTextField.leadingAssistiveLabel.text = "Enter message"
            selectMessageMinLabel.text = "Select message topic"
            
        } else if utils.getLanguage() == "ru" {
            youLetterTextField.label.text = "Ваше сообщение"
            youLetterTextField.leadingAssistiveLabel.text = "Введите сообщение"
            selectMessageMinLabel.text = "Выберите тему сообщения"

        }
        
        youLetterTextField.defaultStyle()
        youLetterTextField.clearButtonMode = .whileEditing
        youLetterTextField.keyboardType = .default

        //theme
        self.view.backgroundColor = Theme.backgroundColorLight
        self.toolbarView.backgroundColor = Theme.backgroundColorLight
        self.contentView.backgroundColor = Theme.backgroundColorLight
        self.contentViewSafeArea.backgroundColor = Theme.backgroundColor
        self.scrollView.backgroundColor = Theme.backgroundColor
        self.selectMessageView.backgroundColor = Theme.backgroundColorLight
        self.youLetterTextField.backgroundColor = Theme.backgroundColorLight
        self.youLetterTextField.textColor = Theme.textColor
        self.continueButton.setTitleColor(Theme.buttonTextColor, for: .normal)
        self.backButton.tintColor = Theme.textColor
        selectMessageImage.image = UIImage(named: "triangle-down_1")?.withRenderingMode(.alwaysTemplate)
        selectMessageImage.tintColor = Theme.textColor
        
        //shadow
        utils.setBottomShadow(toolbarView)
        utils.setShadow(contentView)
    }
}

extension ContactViewController: UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate {

    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topic.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->  UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath)
        cell.textLabel?.text = topic[indexPath.row]
        cell.textLabel?.font = .systemFont(ofSize: 15.0)
        cell.textLabel?.numberOfLines = 2
        cell.backgroundColor = Theme.backgroundColorLight
        cell.textLabel?.textColor = Theme.textColor
    
        return cell
    }
    
    //MARK: UITableViewDelegate
    func tableView (_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 50
    }
    
    func tableView (_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       tableView.deselectRow (at: indexPath, animated: true)
        selectMessageLabel.text = topic[indexPath.row]
        topicToSend = topic[indexPath.row]
   }
    
    //MARK: UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == youLetterTextField {
            youLetterTextField.editingStyle()
        }
        activeField = textField

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == youLetterTextField {
            youLetterTextField.defaultStyle()
        }
        activeField = nil

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text{
            let invalidCharacters =
                CharacterSet ( charactersIn: "QWERTYUIOPASDFGHJKLZXCVBNMÜüqwertyuiopasdfghjklzxcvbnmçäöňşý .-," ) .inverted
      
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
      
            if textField == youLetterTextField {
                if  newString.count < 100 {
                    youLetterTextField.editingStyle()
              return ( string.rangeOfCharacter (from: invalidCharacters) == nil )
                    
                }
                
            }
        }
    return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        youLetterTextField.defaultStyle()
        return true
    }

}

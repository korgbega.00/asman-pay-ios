//
//  Response.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 18.04.2022.
//

import Foundation

struct LoginResponse: Codable {

    let success: Bool
    let data: [String]
}

struct User: Codable {
    let id: Int
    let phone: String
}

struct DataP: Codable {
    let access_token: String
    let user : User
}

struct VerifyOTPResponse: Codable {
    
    let success: Bool
}

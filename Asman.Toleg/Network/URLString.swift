//
//  Net.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 16.04.2022.
//

import Foundation

let URL_BASE = "http://217.174.231.218:9012/api/v1/"

class URLString {
    
    static let URL_LOGIN_USER = URL_BASE + "login/"
    static let URL_VERIFY_OTP = URL_BASE + "verify-otp/"
    static let URL_LOGOUT = URL_BASE + "logout/"
    
}

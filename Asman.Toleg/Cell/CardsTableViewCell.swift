//
//  CardsTableViewCell.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 18.01.2022.
//

import UIKit

class CardsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberCardLabel: UILabel!
    @IBOutlet weak var dateCardsLabel: UILabel!
    @IBOutlet weak var conView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        customizingUI()
        conView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func customizingUI() {
        self.conView.backgroundColor = Theme.backgroundColorLight
        self.contentView.backgroundColor = Theme.backgroundColor
    }

}

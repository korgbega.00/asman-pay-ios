//
//  CardCell.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 26.01.2022.
//

import UIKit

class CardCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = Theme.backgroundColorLight
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

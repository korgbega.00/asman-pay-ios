//
//  CardsBlankCell.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 18.03.2022.
//

import UIKit

class CardsBlankCell: UITableViewCell {

    let utils = Utils()
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var conView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        customizingUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func customizingUI() {
        continueButton.layer.cornerRadius = 20
        conView.layer.cornerRadius = 10
        
        //theme
        contentView.backgroundColor = Theme.backgroundColor
        conView.backgroundColor = Theme.backgroundColorLight
        continueButton.setTitleColor(Theme.buttonTextColor, for: .normal)
        
        //shadow
        utils.setShadow(conView)
    }
}

//
//  BlankCardsAlertCell.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 18.03.2022.
//

import UIKit

class BlankCardsAlertCell: UITableViewCell {

    @IBOutlet weak var conView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        customizingUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func customizingUI() {
        conView.backgroundColor = Theme.backgroundColorLight
    }
}

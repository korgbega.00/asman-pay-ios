//
//  HistoryCell.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 22.12.2021.
//

import UIKit

class HistoryCell: UITableViewCell {
    
    let utils = Utils()
    
    @IBOutlet weak var conView: UIView!
    @IBOutlet weak var cimageView: UIImageView!
    @IBOutlet weak var cimageContentView: UIView!
    @IBOutlet weak var successfullyView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        customizingUI()
        
        self.layer.cornerRadius = 10
//        conView.layer.cornerRadius = 0
        cimageContentView.layer.masksToBounds = true
        cimageContentView.layer.cornerRadius = cimageContentView.bounds.width / 2
        
        successfullyView.layer.cornerRadius = 10
        successfullyView.layer.masksToBounds = true
        successfullyView.layer.cornerRadius = successfullyView.bounds.width / 2
        
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
    private func customizingUI() {
        //Theme
        self.conView.backgroundColor = Theme.backgroundColorLight
        self.headerView.backgroundColor = Theme.backgroundColorLight
    }
  
}

//
//  PYGGViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 26.12.2021.
//

import UIKit

class PyggViewController: UIViewController {

    let utils = Utils()
    
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var safeAreaContentView: UIView!
    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizingUI()
        
        utils.roundCorners(view: toolbarView, radius: 20)
          
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
 
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func customizingUI() {
        self.view.backgroundColor = Theme.backgroundColorLight
        self.toolbarView.backgroundColor = Theme.backgroundColorLight
        self.safeAreaContentView.backgroundColor = Theme.backgroundColor
        self.helpButton.tintColor = Theme.textColor
        self.backButton.tintColor = Theme.textColor
    }

}

//
//  ConfirmationViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 27.12.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class ConfirmationViewController: UIViewController {

    let utils = Utils()
    weak var activeField : UITextField?

    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var textPhoneNumberLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var textAmountLabel: UILabel!
    @IBOutlet weak var textCardNumber: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var smsCodeTextField: MDCOutlinedTextField!
    @IBOutlet weak var contentViewSafeArea: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        smsCodeTextField.delegate = self

        customizingUI()
        
        // TODO: @Copy-pasta
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        view.addGestureRecognizer(tap)
   
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        smsCodeTextField.becomeFirstResponder()
//    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
 
    @IBAction func continueAction(_ sender: Any) {
        utils.validateNumberRange(view: smsCodeTextField, min: 5, max: 5)
        
        if utils.validateNumberRange(view: smsCodeTextField, min: 1, max: 5) {
            //code
            
        }

    }
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //keyboard
    @objc func keyboardDidShow(notification: Notification) {
        print("show keyboard")
        let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        guard let activeField = activeField, var keyboardHeight = keyboardSize?.height else { return }
           
        let tempKeyboardAdditional : CGFloat = 0.0
        let tempMoveAdditional : CGFloat = 0.0
           
        keyboardHeight += tempKeyboardAdditional  // TODO: Temporary hack. Find real value
        let moveViewFromBottomSize = keyboardHeight + tempMoveAdditional
           
           
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: moveViewFromBottomSize, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        let activeRect = activeField.convert(activeField.bounds, to: scrollView)
        scrollView.scrollRectToVisible(activeRect, animated: true)
    }

    @objc func keyboardWillBeHidden(notification: Notification) {
        print("Hide keyboard")
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
       
    @objc func DismissKeyboard(){
        view.endEditing(true)
    }

    func customizingUI() {
        utils.roundCorners(view: toolbarView, radius: 20)
        contentView.layer.cornerRadius = 10
        continueButton.layer.cornerRadius = 20
        
        if utils.getLanguage() == "tk" {
            smsCodeTextField.label.text = "Sms kod"
            smsCodeTextField.leadingAssistiveLabel.text = "Jemi 5 san bolmaly"
            
        } else if utils.getLanguage() == "en" {
            smsCodeTextField.label.text = "Sms code"
            smsCodeTextField.leadingAssistiveLabel.text = "5 digits total"
            
        } else if utils.getLanguage() == "ru" {
            smsCodeTextField.label.text = "Смс код"
            smsCodeTextField.leadingAssistiveLabel.text = "Введите все 5 цифр"
   
        }
        smsCodeTextField.defaultStyle()
        smsCodeTextField.clearButtonMode = .whileEditing
        smsCodeTextField.keyboardType = .numberPad
  
        //theme
        self.view.backgroundColor = Theme.backgroundColorLight
        self.toolbarView.backgroundColor = Theme.backgroundColorLight
        self.contentViewSafeArea.backgroundColor = Theme.backgroundColor
        self.contentView.backgroundColor = Theme.backgroundColorLight
        self.smsCodeTextField.backgroundColor = Theme.backgroundColorLight
        self.smsCodeTextField.textColor = Theme.textColor
        self.scrollContentView.backgroundColor = Theme.backgroundColor
        self.backButton.tintColor = Theme.textColor
        self.continueButton.setTitleColor(Theme.buttonTextColor, for: .normal)
        
        //shadow
        utils.setBottomShadow(toolbarView)
        utils.setShadow(contentView)
     
    }
}
extension ConfirmationViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == smsCodeTextField {
            smsCodeTextField.editingStyle()
        }
        activeField = textField

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == smsCodeTextField {
            smsCodeTextField.defaultStyle()
        }
        activeField = nil

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text{
            let invalidCharacters =
                CharacterSet ( charactersIn: "1234567890" ) .inverted
      
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
      
            if textField == smsCodeTextField {
                if newString.count <= 5 {
                    smsCodeTextField.editingStyle()
                    return ( string.rangeOfCharacter (from: invalidCharacters) == nil )

                }
            }
        }
    return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        smsCodeTextField.defaultStyle()
        return true
    }
}

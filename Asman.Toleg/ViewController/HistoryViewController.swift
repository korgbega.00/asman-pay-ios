//
//  HistoryViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 20.12.2021.
//

import UIKit

class HistoryViewController: UIViewController {

    var x = CGFloat()
    var y = CGFloat()
    let utils = Utils()
    var button = UIButton()
    var bool = true
    let LANGUAGE_KEY = "CurrentLanguage"
    let defaults = UserDefaults.standard
    var currentLanguage = "ru"
    let refreashControl = UIRefreshControl()
    let bigVarningLabel = UILabel()
    let smallVarningLabel = UILabel()
    var myIndexPathRow = 0

    
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var historyTableView: UITableView!
    @IBOutlet weak var safeAreaContentView: UIView!
    @IBOutlet weak var bigWarningLabel: UILabel!
    @IBOutlet weak var smallWarningLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreashControl.transform = CGAffineTransform(scaleX: 0.7, y: 0.7);
        refreashControl.tintColor = Theme.textColor
        self.historyTableView.addSubview(refreashControl)
        
        customizingUI()
                
        historyTableView.dataSource = self
        historyTableView.delegate = self
        
        historyTableView.showsVerticalScrollIndicator = false
        historyTableView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
        utils.roundCorners(view: toolbarView, radius: 20)
                
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tableViewTapped(recognizer:)))
        historyTableView.addGestureRecognizer(tapGestureRecognizer)

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
    
    @objc func buttonAction(sender : UIButton) {
        self.bool = true
        self.button.removeFromSuperview()
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var deleteHistoryViewController : RemoveHistoryAlertController!
        
        deleteHistoryViewController = storyboard.instantiateViewController(withIdentifier: "DeleteHistoryViewControllerID") as? RemoveHistoryAlertController

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alertController.setValue(deleteHistoryViewController, forKey: "contentViewController")
        self.present(alertController, animated: true, completion: nil)
    }
        
    @objc func tableViewTapped(recognizer: UITapGestureRecognizer) {
        refreashControl.endRefreshing()
        let location = recognizer.location(in: self.historyTableView) // point of touch in tableView
        if let indexPath = self.historyTableView.indexPathForRow(at: location) { // indexPath of touch location
            myIndexPathRow = indexPath.row
            if let cell = historyTableView.cellForRow(at: indexPath) as? HistoryCell {
                let locationInCell = recognizer.location(in: cell) // point of touch in cell
                // do something with location or locationInCell

                if cell.contentView.frame.contains(location) || cell.contentView.frame.contains(locationInCell) {
                    if bool {
                        print(indexPath.row)
                        myIndexPathRow = indexPath.row
                        historyTableView.reloadData()
                        self.x = location.x
                        self.y = location.y
                        if indexPath.row == 0 && cell.center.y > y { return }
                        if location.x > cell.center.x { self.x = location.x - cell.center.x }
                        if location.y < cell.center.y { self.y = location.y + 10.0 }
                        if location.y > cell.center.y { self.y = location.y - 40.0 }
                        
                        if let  language = defaults.string(forKey: LANGUAGE_KEY) {
                            currentLanguage = language
                        }
                        if currentLanguage == "tk" {
                            self.button = showMenu(point: CGPoint(x: self.x, y: self.y), contentView: self.historyTableView, title: " Töleglerden poz")
                        }
                        if currentLanguage == "en" {
                            self.button = showMenu(point: CGPoint(x: self.x, y: self.y), contentView: self.historyTableView, title: " Remove from history")
                        }
                        if currentLanguage == "ru" {
                            self.button = showMenu(point: CGPoint(x: self.x, y: self.y), contentView: self.historyTableView, title: " Удалить из истории")
                        }

                        self.historyTableView.addSubview(button)
                        button.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
                        self.bool = false } else {
                            self.button.removeFromSuperview()
                            self.bool = true
                        }
                }
            }
        }
    }
    
    private func customizingUI() {
        self.view.backgroundColor = Theme.backgroundColorLight
        safeAreaContentView.backgroundColor = Theme.backgroundColor
        toolbarView.backgroundColor = Theme.backgroundColorLight
        historyTableView.backgroundColor = Theme.backgroundColor
        historyTableView.separatorColor = Theme.lineColor
        
        //shadow
        utils.setBottomShadow(toolbarView)
        
    }

}
extension HistoryViewController: UITableViewDataSource,UITableViewDelegate{
    
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->  UITableViewCell {
        let cell = historyTableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as! HistoryCell
        if indexPath.row == 0 {
            cell.headerHeight.constant = 43
        } else {
            cell.headerHeight.constant = 0 }
        
//        if indexPath.row == myIndexPathRow {
//            cell.conView.backgroundColor = .black
//        } else {
//            cell.conView.backgroundColor = .white
//        }
        
        return cell
    }
    
    //MARK: UITableViewDelegate
    func tableView (_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 123
        } else {
            return 80
        }
    }
    
    func tableView (_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       tableView.deselectRow (at: indexPath, animated: true)
        print(indexPath.row)
   }
    
}

//
//  SettingsViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 20.12.2021.
//

import UIKit

class SettingsViewController: UIViewController {
    
    let LANGUAGE_KEY = "CurrentLanguage"
    let defaults = UserDefaults.standard
    var currentLanguage = "ru"
    let utils = Utils()
    var switchState = false
    

    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var view_2: UIView!
    @IBOutlet weak var view_3: UIView!
    @IBOutlet weak var setAppKeyButton: UIButton!
    @IBOutlet weak var changeAppLangButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var safeAreaContentView: UIView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var mySwitch: UISwitch!
    @IBOutlet weak var languageTitleLabel: UILabel!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizingUI()
        
        
        if let language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        let appCode = UserDefaults.standard.string(forKey: "appCode")
        if let language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        if currentLanguage == "tk" {
            changeAppLangButton.setTitle("Türkmençe", for: .normal)
            languageTitleLabel.text = " Dil "

        } else if currentLanguage == "en" {
            changeAppLangButton.setTitle("English", for: .normal)
            languageTitleLabel.text = " Language "

            
        } else if currentLanguage == "ru" {
            changeAppLangButton.setTitle("Pусский", for: .normal)
            languageTitleLabel.text = " Язык "

        }
               
        utils.roundCorners(view: toolbarView, radius: 20)
        backButton.layer.cornerRadius = 20
        
        view_1.layer.cornerRadius = 10
        view_2.layer.cornerRadius = 10
        view_3.layer.cornerRadius = 10
        
        setAppKeyButton.layer.borderWidth = 1
        setAppKeyButton.layer.cornerRadius = 5
        //C8CACB
        setAppKeyButton.layer.borderColor = CGColor(red: CGFloat(0xC8)/255, green: (0xCA)/255, blue: (0xCB)/255, alpha: 1.0)
        
        if appCode == nil {
            if currentLanguage == "tk" {
                setAppKeyButton.setTitle("Açar söz goý", for: .normal)

            } else if currentLanguage == "en" {
                setAppKeyButton.setTitle("Set passcode", for: .normal)
                
            } else if currentLanguage == "ru" {
                setAppKeyButton.setTitle("Установить код блокировки", for: .normal)

            }
        } else {
            if currentLanguage == "tk" {
                setAppKeyButton.setTitle("Açar sözi poz", for: .normal)

            } else if currentLanguage == "en" {
                setAppKeyButton.setTitle("Remove passcode", for: .normal)
                
            } else if currentLanguage == "ru" {
                setAppKeyButton.setTitle("Удалить код блокировки", for: .normal)

            }
        }
      
        changeAppLangButton.layer.borderWidth = 1
        changeAppLangButton.layer.cornerRadius = 5
        //C8CACB
        changeAppLangButton.layer.borderColor = CGColor(red: CGFloat(0xC8)/255, green: (0xCA)/255, blue: (0xCB)/255, alpha: 1.0)
      

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
    @IBAction func addAppKeyAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var setAppKeyAlertController : SetAppKeyAlertController!
        
        setAppKeyAlertController = storyboard.instantiateViewController(withIdentifier: "SetAppKeyAlertControllerID") as? SetAppKeyAlertController
        setAppKeyAlertController.delegate = self
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alertController.setValue(setAppKeyAlertController, forKey: "contentViewController")
        self.present(alertController, animated: true, completion: nil)

    }
    @IBAction func changeAppLanguageAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var appLanguageAlertController : AppLanguageAlertController!
        
        appLanguageAlertController = storyboard.instantiateViewController(withIdentifier: "alertLanguageSettingsID") as? AppLanguageAlertController
        appLanguageAlertController.delegate = self

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alertController.setValue(appLanguageAlertController, forKey: "contentViewController")
        self.present(alertController, animated: true, completion: nil)

    }
    
    @objc func changeTheme(sender: UISwitch) {
        if !sender.isOn {
            print("sender = \(sender.isOn) switch = \(sender.isOn)")
            Theme.defaultTheme()
            defaults.set(sender.isOn, forKey: "theme")
            let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "tabBarId") as? UITabBarController
            self.view.window?.rootViewController = homeViewController
            homeViewController?.selectedViewController = homeViewController?.viewControllers?[3]
        } else {
            print("sender = \(sender.isOn) switch = \(sender.isOn)")

            Theme.darkTheme()
            defaults.set(sender.isOn, forKey: "theme")
            let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "tabBarId") as? UITabBarController
            self.view.window?.rootViewController = homeViewController
            homeViewController?.selectedViewController = homeViewController?.viewControllers?[3]        }

    }
    
    private func customizingUI() {
        switchState = UserDefaults.standard.bool(forKey: "theme")
        mySwitch.isOn = switchState
        self.mySwitch.clipsToBounds = true
        self.mySwitch.layer.cornerRadius = mySwitch.frame.height / 2.0
        self.mySwitch.addTarget(self, action: #selector(changeTheme(sender:)), for: .valueChanged)
        self.languageTitleLabel.backgroundColor = Theme.backgroundColorLight

        self.safeAreaContentView.backgroundColor = Theme.backgroundColor
        self.view.backgroundColor = Theme.backgroundColorLight
        self.toolbarView.backgroundColor = Theme.backgroundColorLight
        self.scrollContentView.backgroundColor = Theme.backgroundColor
        self.view_1.backgroundColor = Theme.backgroundColorLight
        self.view_2.backgroundColor = Theme.backgroundColorLight
     
        self.view_3.backgroundColor = Theme.backgroundColorLight
        self.backButton.setTitleColor(Theme.buttonTextColor, for: .normal)
        
        //shadow
        utils.setShadow(view_1)
        utils.setShadow(view_2)
        utils.setShadow(view_3)
        utils.setBottomShadow(toolbarView)
    
     
    }

    @IBAction func goBack(_ sender: Any) {
        print("Exit")
        exit(0)
    }

}
protocol SettingsViewControllerDelegate : AnyObject {
    func setKey()
    func changeLanguage()
}

extension SettingsViewController : SettingsViewControllerDelegate {
    func changeLanguage() {
        if let language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        if currentLanguage == "tk" {
            changeAppLangButton.setTitle("Türkmençe", for: .normal)

        } else if currentLanguage == "en" {
            changeAppLangButton.setTitle("English", for: .normal)
            
        } else if currentLanguage == "ru" {
            changeAppLangButton.setTitle("Pусский", for: .normal)
        }

    }
    
    func setKey() {
        let appCode = UserDefaults.standard.string(forKey: "appCode")
        if let language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
          if appCode == nil {
            if currentLanguage == "tk" {
                setAppKeyButton.setTitle("Açar söz goý", for: .normal)

            } else if currentLanguage == "en" {
                setAppKeyButton.setTitle("Set passcode", for: .normal)
                
            } else if currentLanguage == "ru" {
                setAppKeyButton.setTitle("Установить код блокировки", for: .normal)
            }
            
          } else {
              if currentLanguage == "tk" {
                  setAppKeyButton.setTitle("Açar sözi poz", for: .normal)

              } else if currentLanguage == "en" {
                  setAppKeyButton.setTitle("Remove passcode", for: .normal)
                  
              } else if currentLanguage == "ru" {
                  setAppKeyButton.setTitle("Удалить код блокировки", for: .normal)
                  
              }
            
        }
        
    }
    
}

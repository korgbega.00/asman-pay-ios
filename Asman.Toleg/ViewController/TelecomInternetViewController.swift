//
//  TelecomInternetViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 26.12.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class TelecomInternetViewController: UIViewController {

    let utils = Utils()
    var gestureRegonizersMessage = UITapGestureRecognizer()
    var card: Card? = nil
    weak var activeField : UITextField?

    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var selectCardView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var phoneNumberTextField: MDCOutlinedTextField!
    @IBOutlet weak var amountTextField: MDCOutlinedTextField!
    @IBOutlet weak var contentViewSafeArea: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var help2Button: LocalizableButton!
    @IBOutlet weak var selectedViewLabel: UILabel!
    @IBOutlet weak var selectedViewTitle: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var selectCardImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        phoneNumberTextField.delegate = self
        amountTextField.delegate = self
             
        customizingUI()
        
        utils.roundCorners(view: toolbarView, radius: 20)
        contentView.layer.cornerRadius = 10
        continueButton.layer.cornerRadius = 20
        
        selectCardView.layer.borderWidth = 1
        selectCardView.layer.cornerRadius = 5
        defaultSelectedView()
        gestureRegonizersMessage = UITapGestureRecognizer(target: self, action: #selector(chooseCardAction(_:)))
        selectCardView.addGestureRecognizer(gestureRegonizersMessage)
     
        // TODO: @Copy-pasta
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        view.addGestureRecognizer(tap)
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        phoneNumberTextField.becomeFirstResponder()
    }
   
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
    
    @IBAction func continueAction(_ sender: Any) {
        utils.validateNumberRange(view: amountTextField, min: 1, max: 4)
        utils.validateNumberRange(view: phoneNumberTextField, min: 8, max: 8)
        if card == nil {
            errorSelectedView()
        }
        
        if utils.validateNumberRange(view: phoneNumberTextField, min: 8, max: 8) && utils.validateNumberRange(view: amountTextField, min: 1, max: 4) && card != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            var webViewController : WebViewController!
            webViewController = storyboard.instantiateViewController(withIdentifier: "WebViewID") as? WebViewController
            self.present(webViewController, animated: true, completion: nil)
        } else {

        }
    }
    @IBAction func helpToolbarAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HelpViewControllerID") as! HelpViewController
        vc.id = "TelecomInternetViewController"
        present(vc, animated: true, completion: nil)
 
    }
    
    @IBAction func helpAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HelpViewControllerID") as! HelpViewController
        vc.id = "TelecomInternetViewController"
        present(vc, animated: true, completion: nil)

    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //keyboard
    @objc func keyboardDidShow(notification: Notification) {
        print("show keyboard")
        let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        guard let activeField = activeField, var keyboardHeight = keyboardSize?.height else { return }
           
        let tempKeyboardAdditional : CGFloat = 0.0
        let tempMoveAdditional : CGFloat = 0.0
           
        keyboardHeight += tempKeyboardAdditional  // TODO: Temporary hack. Find real value
        let moveViewFromBottomSize = keyboardHeight + tempMoveAdditional
           
           
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: moveViewFromBottomSize, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        let activeRect = activeField.convert(activeField.bounds, to: scrollView)
        scrollView.scrollRectToVisible(activeRect, animated: true)
    }

    @objc func keyboardWillBeHidden(notification: Notification) {
        print("Hide keyboard")
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
       
    @objc func DismissKeyboard(){
        view.endEditing(true)
    }
    
    @objc func chooseCardAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var chooseCardAlertController : ChooseCardAlertController!
        
        chooseCardAlertController = storyboard.instantiateViewController(withIdentifier: "ChooseCardAlertControllerID") as? ChooseCardAlertController
        chooseCardAlertController.telecomInternet = self
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alertController.setValue(chooseCardAlertController, forKey: "contentViewController")
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func errorSelectedView(){
        //red
        selectCardView.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
        selectedViewLabel.textColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
        selectedViewLabel.alpha = 1
    }
    
    public func defaultSelectedView(){
        //C8CACB
        selectCardView.layer.borderColor = CGColor(red: CGFloat(0xC8)/255, green: (0xCA)/255, blue: (0xCB)/255, alpha: 1.0)
        selectedViewLabel.alpha = 0
    }
    
    private func customizingUI() {
        if utils.getLanguage() == "tk" {
            phoneNumberTextField.label.text = "Öý telefon belginizi giriziň"
            phoneNumberTextField.leadingAssistiveLabel.text = "Meselem: (kod+belgi) 12XXXXXX ýa-da 137XXXXX"
            amountTextField.label.text = "Mukdar"
            amountTextField.leadingAssistiveLabel.text = "Kartdan alynjak mukdar"
            selectedViewLabel.text = "Ulanyljak karty saýla"

        } else if utils.getLanguage() == "en" {
            phoneNumberTextField.label.text = "Enter home phone number"
            phoneNumberTextField.leadingAssistiveLabel.text = "For example: (code+number) 12XXXXXX or 137XXXXX"
            amountTextField.label.text = "Amount"
            amountTextField.leadingAssistiveLabel.text = "Amount to be charged"
            selectedViewLabel.text = "To get the amount from"

        } else if utils.getLanguage() == "ru" {
            phoneNumberTextField.label.text = "Введите номер дом.телефона"
            phoneNumberTextField.leadingAssistiveLabel.text = "Например: (код+номер) 12XXXXXX или 137XXXXX"
            amountTextField.label.text = "Количество"
            amountTextField.leadingAssistiveLabel.text = "Сумма снимается с карты"
            selectedViewLabel.text = "С которой снимаются деньги"

        }
        phoneNumberTextField.defaultStyle()
        amountTextField.defaultStyle()
        phoneNumberTextField.clearButtonMode = .whileEditing
        phoneNumberTextField.keyboardType = .phonePad
        amountTextField.clearButtonMode = .whileEditing
        amountTextField.keyboardType = .numberPad

        //theme
        self.view.backgroundColor = Theme.backgroundColorLight
        self.contentViewSafeArea.backgroundColor = Theme.backgroundColor
        self.scrollContentView.backgroundColor = Theme.backgroundColor
        self.toolbarView.backgroundColor = Theme.backgroundColorLight
        self.contentView.backgroundColor = Theme.backgroundColorLight
        self.backButton.tintColor = Theme.textColor
        self.helpButton.tintColor = Theme.textColor
        self.phoneNumberTextField.backgroundColor = Theme.backgroundColorLight
        self.phoneNumberTextField.textColor = Theme.textColor
        self.amountTextField.backgroundColor = Theme.backgroundColorLight
        self.amountTextField.textColor = Theme.textColor
        self.selectCardView.backgroundColor = Theme.backgroundColorLight
        self.continueButton.setTitleColor(Theme.buttonTextColor, for: .normal)
        //#3EA3EB
        self.help2Button.setTitleColor(UIColor(cgColor: CGColor(red: CGFloat(0x3E)/255, green: CGFloat(0xA3)/255, blue: CGFloat(0xEB)/255, alpha: 1)), for: .normal)
        selectCardImage.image = UIImage(named: "triangle-down_1")?.withRenderingMode(.alwaysTemplate)
        selectCardImage.tintColor = Theme.textColor

        
        //shadow
        utils.setBottomShadow(toolbarView)
        utils.setShadow(contentView)
    }
    
}
extension TelecomInternetViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phoneNumberTextField {
            phoneNumberTextField.editingStyle()
        }
        if textField == amountTextField {
            amountTextField.editingStyle()
        }
        activeField = textField

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == phoneNumberTextField {
            phoneNumberTextField.defaultStyle()
        }
        if textField == amountTextField {
            amountTextField.defaultStyle()
        }
        activeField = nil

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text{
            let invalidCharacters =
                CharacterSet ( charactersIn: "1234567890" ) .inverted
      
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
      
 
            if textField == phoneNumberTextField {
                if  newString.count <= 8 {
                    phoneNumberTextField.editingStyle()
                    return ( string.rangeOfCharacter (from: invalidCharacters) == nil )
                }
            }
            
            if textField == amountTextField {
                if newString.count <= 4 {
                    amountTextField.editingStyle()
                    return ( string.rangeOfCharacter (from: invalidCharacters) == nil )

                }
            }
        }
    return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        phoneNumberTextField.defaultStyle()
        amountTextField.defaultStyle()
        return true
    }
}


//
//  HelpViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 26.01.2022.
//

import UIKit

class HelpViewController: UIViewController {


    let utils = Utils()
    var id = ""
    let LANGUAGE_KEY = "CurrentLanguage"
    let defaults = UserDefaults.standard
    var currentLanguage = "ru"
    
    @IBOutlet weak var toolBarView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var titleFirstItem: UILabel!
    @IBOutlet weak var subtitleFirstItem: UILabel!
    @IBOutlet weak var oneView: UIView!
    @IBOutlet weak var numbericLabelFirstItem: UILabel!
    @IBOutlet weak var textLabelFirstItem: UILabel!
    @IBOutlet weak var imageFirstItem: UIImageView!
    @IBOutlet weak var lineFirstItem: UIView!
    
    
    @IBOutlet weak var titleSecondItem: UILabel!
    @IBOutlet weak var subtitleSecondItem: UILabel!
    @IBOutlet weak var twoView: UIView!
    @IBOutlet weak var numbericLabelSecondItem: UILabel!
    @IBOutlet weak var textLabelSecondItem: UILabel!
    @IBOutlet weak var imageSecondItem: UIImageView!
    @IBOutlet weak var lineSecondItem: UIView!
    
    @IBOutlet weak var mycardsButtonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var titleThirdItem: UILabel!
    @IBOutlet weak var subtitleThirdItem: UILabel!
    @IBOutlet weak var threeView: UIView!
    @IBOutlet weak var numbericLabelThirdItem: UILabel!
    @IBOutlet weak var textLabelThirdItem: UILabel!
    @IBOutlet weak var imageThirdItem: UIImageView!
    @IBOutlet weak var buttonThirdItem: UIButton!
    @IBOutlet weak var safeAreaContentView: UIView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var heightSecondLine: NSLayoutConstraint!
    @IBOutlet weak var heightSecondImage: NSLayoutConstraint!
    @IBOutlet weak var heightThirdImage: NSLayoutConstraint!
    @IBOutlet weak var heightFirstImage: NSLayoutConstraint!
    @IBOutlet weak var heightFirstLine: NSLayoutConstraint!
    @IBOutlet weak var selectCardImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
    
        
        customizationView()
        
        if id == "NewCardViewController" {
            customizationViewforNewCard()
        } else if id == "TMCellViewController" {
            customizationViewforTMCell()
        } else if id == "TelecomInternetViewController" {
            customizationViewforTelecomInternet()
        } else {
            customizationViewforOther()
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewCardViewControllerID") as! NewCardViewController
        present(vc, animated: true, completion: nil)
    }
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func customizationView() {
        utils.roundCorners(view: toolBarView, radius: 20)
        contentView.layer.cornerRadius = 20
        
        subtitleFirstItem.backgroundColor = .white
        oneView.layer.borderWidth = 1
        oneView.layer.cornerRadius = 5
        //C8CACB
        oneView.layer.borderColor = CGColor(red: CGFloat(0xC8)/255, green: (0xCA)/255, blue: (0xCB)/255, alpha: 1.0)
        oneView.backgroundColor = .systemBackground
      
        twoView.layer.borderWidth = 1
        twoView.layer.cornerRadius = 5
        //C8CACB
        twoView.layer.borderColor = CGColor(red: CGFloat(0xC8)/255, green: (0xCA)/255, blue: (0xCB)/255, alpha: 1.0)
        twoView.backgroundColor = .systemBackground
      
        threeView.layer.borderWidth = 1
        threeView.layer.cornerRadius = 5
        //C8CACB
        threeView.layer.borderColor = CGColor(red: CGFloat(0xC8)/255, green: (0xCA)/255, blue: (0xCB)/255, alpha: 1.0)
        threeView.backgroundColor = .systemBackground
        
        //Theme
        self.view.backgroundColor = Theme.backgroundColorLight
        self.toolBarView.backgroundColor = Theme.backgroundColorLight
        self.safeAreaContentView.backgroundColor = Theme.backgroundColor
        self.contentView.backgroundColor = Theme.backgroundColorLight
        self.scrollContentView.backgroundColor = Theme.backgroundColor
        self.backButton.tintColor = Theme.textColor
        self.oneView.backgroundColor = Theme.backgroundColorLight
        self.twoView.backgroundColor = Theme.backgroundColorLight
        self.threeView.backgroundColor = Theme.backgroundColorLight
        self.subtitleFirstItem.backgroundColor = Theme.backgroundColorLight
        self.subtitleSecondItem.backgroundColor = Theme.backgroundColorLight
        self.subtitleThirdItem.backgroundColor = Theme.backgroundColorLight
        //2888E5
        self.titleFirstItem.textColor = UIColor(cgColor: CGColor(red: CGFloat(0x28)/255, green: CGFloat(0x88)/255, blue: CGFloat(0xE5)/255, alpha: 1))
        self.titleThirdItem.textColor = UIColor(cgColor: CGColor(red: CGFloat(0x28)/255, green: CGFloat(0x88)/255, blue: CGFloat(0xE5)/255, alpha: 1))
        self.titleSecondItem.textColor = UIColor(cgColor: CGColor(red: CGFloat(0x28)/255, green: CGFloat(0x88)/255, blue: CGFloat(0xE5)/255, alpha: 1))
        //3EA3EB
        self.buttonThirdItem.setTitleColor(UIColor(cgColor: CGColor(red: CGFloat(0x3E)/255, green: CGFloat(0xA3)/255, blue: CGFloat(0xEB)/255, alpha: 1)), for: .normal)
        selectCardImage.image = UIImage(named: "triangle-down_1")?.withRenderingMode(.alwaysTemplate)
        selectCardImage.tintColor = Theme.textColor
        
        //shadow
        utils.setBottomShadow(toolBarView)
        utils.setShadow(contentView)
        
    }
    
    
    private func customizationViewforNewCard(){
        if currentLanguage == "tk" {
            titleFirstItem.text = "Kart belgisini ýazyň"
            subtitleFirstItem.text = " Kart belgisi "
            numbericLabelFirstItem.text = "9934 1234 5678 9101"
            textLabelFirstItem.text = "Kart belgisi, bank kartyňyzyň iň ýokarsynda yerleşýan 16 sandyr."
            imageFirstItem.alpha = 1.0
            
            titleSecondItem.text = "Kartyň möhletini ýazyň"
            subtitleSecondItem.text = " Kartyň möhleti (Aý/Ýyl) "
            numbericLabelSecondItem.text = "01/40"
            textLabelSecondItem.text = "Kartyň möhleti, kart belgisiniň aşaky sag gapdalynda , arasy '/' bilen bölünen 4 sandyr."
            imageSecondItem.alpha = 1.0
            
            titleThirdItem.text = "Kartyň eýesiniň adyny ýazyň"
            subtitleThirdItem.text = " Familiýasy Ady "
            numbericLabelThirdItem.text = "Saparow Emin"
            textLabelThirdItem.text = "Kartyň eýesiniň adyny, bank kartyňyzyň iň aşagynda ýerleşyär"
            imageThirdItem.alpha = 1.0
//            textLabel2ThirdItem.text = "Kartyň eýesiniň adyny, bank kartyňyzyň iň aşagynda ýerleşyär"
            buttonThirdItem.alpha =  0.0
//            topConstraintButtonThirdItem.constant = 0
//            topConstraintTextLAbelThirdItem.constant = 0

        } else if currentLanguage == "en" {
            titleFirstItem.text = "Enter the card number"
            subtitleFirstItem.text = " Card number "
            numbericLabelFirstItem.text = "9934 1234 5678 9101"
            textLabelFirstItem.text = "Card number is a 16-digit number located on the top most part of your bank card"
            imageFirstItem.alpha = 1.0
            
            titleSecondItem.text = "Enter the card expiration date"
            subtitleSecondItem.text = " Card expiration (Month/Year) "
            numbericLabelSecondItem.text = "01/40"
            textLabelSecondItem.text = "Expiration date is a 4-digit number divided by a '/' symbol located on the lower right side of your bank card"
            imageSecondItem.alpha = 1.0
            
            titleThirdItem.text = "Enter the card holder name"
            subtitleThirdItem.text = " Surname name "
            numbericLabelThirdItem.text = "Saparow Emin"
            textLabelThirdItem.text = "Card holder name is the name and surname located at the bottom of your bank card"
            imageThirdItem.alpha = 1.0
//            textLabel2ThirdItem.text = "Card holder name is the name and surname located at the bottom of your bank card"
            buttonThirdItem.alpha =  0.0
//            topConstraintButtonThirdItem.constant = 0
//            topConstraintTextLAbelThirdItem.constant = 0
        } else if currentLanguage == "ru" {
            titleFirstItem.text = "Введите номер банковской карты"
            subtitleFirstItem.text = " Номер карты "
            numbericLabelFirstItem.text = "9934 1234 5678 9101"
            textLabelFirstItem.text = "Номер карты — это 16 - значный номер, расположенный в верхней части вашей банковской карты."
            imageFirstItem.alpha = 1.0
            
            titleSecondItem.text = "Укажите срок действия карты"
            subtitleSecondItem.text = " Срок окончание (Месяц/Год) "
            numbericLabelSecondItem.text = "01/40"
            textLabelSecondItem.text = "Срок действия карты находится под номером карты"
            imageSecondItem.alpha = 1.0
            
            titleThirdItem.text = "Введите имя держателя карты"
            subtitleThirdItem.text = " Фамилия Имя "
            numbericLabelThirdItem.text = "Saparow Emin"
            textLabelThirdItem.text = "Имя держателя карты указано в нижней части вашей банковской карты."
            imageThirdItem.alpha = 1.0
//            textLabel2ThirdItem.text = "Имя держателя карты указано в нижней части вашей банковской карты."
            buttonThirdItem.alpha =  0.0
//            topConstraintButtonThirdItem.constant = 0
//            topConstraintTextLAbelThirdItem.constant = 0
            mycardsButtonHeight.constant = 0
        }
      
    }
    private func customizationViewforTMCell(){
        heightSecondLine.constant = 100
        heightFirstLine.constant = 100
        heightSecondImage.constant = 0
        heightFirstImage.constant = 0
        heightThirdImage.constant = 0
        
        if currentLanguage == "tk" {
            titleFirstItem.text = "Pul salynjak telefon belgisini ýazyň"
            subtitleFirstItem.text = " Telefon belgisi "
            numbericLabelFirstItem.text = "65 260720"
            textLabelFirstItem.text = ""
            imageFirstItem.alpha = 1.0
            
            titleSecondItem.text = "Iberiljek pul mukdaryny ýazyň"
            subtitleSecondItem.text = " Mukdar "
            numbericLabelSecondItem.text = "15"
            textLabelSecondItem.text = ""
            imageSecondItem.alpha = 1.0
            
            titleThirdItem.text = "Töleg üçin ulanyljak bank kartyny saýlaň"
            subtitleThirdItem.text = " Karty saýla "
            numbericLabelThirdItem.text = "**** **** **** 4226"
            textLabelThirdItem.text = "Bu kartyny 'Kartlarym' bölüminde goşup bilersiňiz."
            imageThirdItem.alpha = 1.0
            buttonThirdItem.alpha =  1.0
            buttonThirdItem.setTitle("<<Kartlarym>>", for: .normal)
            
        } else if currentLanguage == "en" {
            titleFirstItem.text = "Enter the phone number to top up"
            subtitleFirstItem.text = " Phone number "
            numbericLabelFirstItem.text = "65 260720"
            textLabelFirstItem.text = ""
            imageFirstItem.alpha = 1.0
            
            titleSecondItem.text = "Enter the amount to pay"
            subtitleSecondItem.text = " Amount "
            numbericLabelSecondItem.text = "15"
            textLabelSecondItem.text = ""
            imageSecondItem.alpha = 1.0
            
            titleThirdItem.text = "Select a bank card for payment"
            subtitleThirdItem.text = " Select a card "
            numbericLabelThirdItem.text = "**** **** **** 4226"
            textLabelThirdItem.text = "You can add a new card in:"
            imageThirdItem.alpha = 1.0
            buttonThirdItem.alpha =  1.0
            buttonThirdItem.setTitle("<<My Cards>>", for: .normal)
            
        } else if currentLanguage == "ru" {
            titleFirstItem.text = "Введите номер для пополнения"
            subtitleFirstItem.text = " Телефон "
            numbericLabelFirstItem.text = "65 260720"
            textLabelFirstItem.text = ""
            imageFirstItem.alpha = 1.0
            titleSecondItem.text = "Укажите сумму оплаты"
            subtitleSecondItem.text = " Количество "
            numbericLabelSecondItem.text = "15"
            textLabelSecondItem.text = ""
            imageSecondItem.alpha = 1.0
                      
            titleThirdItem.text = "Выберите банковскую карту для оплаты"
            subtitleThirdItem.text = " Выберите карту "
            numbericLabelThirdItem.text = "**** **** **** 4226"
            textLabelThirdItem.text = "Вы можете добавить банковскую карту на вкладке :"
            imageThirdItem.alpha = 1.0
            buttonThirdItem.alpha =  1.0
            buttonThirdItem.setTitle("<<Мои карты>>", for: .normal)
              
        
        }
      
    }

    private func customizationViewforTelecomInternet(){
        heightSecondLine.constant = 100
        heightFirstLine.constant = textLabelFirstItem.frame.height + 100
        heightSecondImage.constant = 0
        heightFirstImage.constant = 0
        heightThirdImage.constant = 0
        
        if currentLanguage == "tk" {
            titleFirstItem.text = "Öý telefon belgisini ýazyň"
            subtitleFirstItem.text = " Öý telefon belgisini giriziň "
            numbericLabelFirstItem.text = "12123456"
            textLabelFirstItem.text = "Öý telefon belgisini ýazanyňyzda şaher kody bilen ýazyň. Meselem Änew üçin: 13734567, Daşaguz üçin: 32256789"
            imageFirstItem.alpha = 1.0
            
            titleSecondItem.text = "Iberiljek pul mukdaryny ýazyñ"
            subtitleSecondItem.text = " Mukdar "
            numbericLabelSecondItem.text = "15"
            textLabelSecondItem.text = ""
            imageSecondItem.alpha = 1.0
            
            titleThirdItem.text = "Töleg üçin ulanyljak bank kartyny saýlaň"
            subtitleThirdItem.text = " Karty saýla "
            numbericLabelThirdItem.text = "**** **** **** 4226"
            textLabelThirdItem.text = "Bu kartyny 'Kartlarym' bölüminde goşup bilersiňiz."
            imageThirdItem.alpha = 1.0
            buttonThirdItem.alpha =  1.0
            buttonThirdItem.setTitle("<<Kartlarym>>", for: .normal)
            
        } else if currentLanguage == "en" {
            titleFirstItem.text = "Enter home phone number"
            subtitleFirstItem.text = " Enter home phone number "
            numbericLabelFirstItem.text = "12123456"
            textLabelFirstItem.text = "Phone number has to be entered with a city code. Example: for Annau: 13734567, for Dashoguz: 32256789"
            imageFirstItem.alpha = 1.0
            
            titleSecondItem.text = "Enter the amount to pay"
            subtitleSecondItem.text = " Amount "
            numbericLabelSecondItem.text = "15"
            textLabelSecondItem.text = ""
            imageSecondItem.alpha = 1.0
            
            titleThirdItem.text = "Select a bank card for payment"
            subtitleThirdItem.text = " Select a card "
            numbericLabelThirdItem.text = "**** **** **** 4226"
            textLabelThirdItem.text = "You can add a new card in :"
            imageThirdItem.alpha = 1.0
            buttonThirdItem.alpha =  1.0
            buttonThirdItem.setTitle("<<My Cards>>", for: .normal)
            
        } else if currentLanguage == "ru" {
            titleFirstItem.text = "Введите номер домашнего телефона"
            subtitleFirstItem.text = " Введите номер дом.телефона "
            numbericLabelFirstItem.text = "65 260720"
            textLabelFirstItem.text = "Номер телефона необходимо вводить с кодом города. Пример: для Аннау: 13734567, для Дашогуза: 32256789"
            imageFirstItem.alpha = 1.0
            
            titleSecondItem.text = "Укажите сумму оплаты"
            subtitleSecondItem.text = " Количество "
            numbericLabelSecondItem.text = "15"
            textLabelSecondItem.text = ""
            imageSecondItem.alpha = 1.0
                      
            titleThirdItem.text = "Выберите банковскую карту для оплаты"
            subtitleThirdItem.text = " Выберите карту "
            numbericLabelThirdItem.text = "**** **** **** 4226"
            textLabelThirdItem.text = "Вы можете добавить банковскую карту на вкладке :"
            imageThirdItem.alpha = 1.0
            buttonThirdItem.alpha =  1.0
            buttonThirdItem.setTitle("<<Мои карты>>", for: .normal)
        }
      
    }
    private func customizationViewforOther(){
        heightSecondLine.constant = 100
        heightFirstLine.constant = 100
        heightSecondImage.constant = 0
        heightFirstImage.constant = 0
        heightThirdImage.constant = 0
        
        if currentLanguage == "tk" {
            titleFirstItem.text = "Öý telefon belgisini ýazyñ"
            subtitleFirstItem.text = " Öý telefon belgisini giriziň "
            numbericLabelFirstItem.text = "412345"
            textLabelFirstItem.text = ""
            imageFirstItem.alpha = 1.0
            
            titleSecondItem.text = "Iberiljek pul mukdaryny ýazyñ"
            subtitleSecondItem.text = " Mukdar "
            numbericLabelSecondItem.text = "15"
            textLabelSecondItem.text = ""
            imageSecondItem.alpha = 1.0
            
            titleThirdItem.text = "Töleg üçin ulanyljak bank kartyny saýlaň"
            subtitleThirdItem.text = " Karty saýla "
            numbericLabelThirdItem.text = "**** **** **** 4226"
            textLabelThirdItem.text = "Bu kartyny 'Kartlarym' bölüminde goşup bilersiňiz."
            imageThirdItem.alpha = 1.0
            buttonThirdItem.alpha =  1.0
            buttonThirdItem.setTitle("<<Kartlarym>>", for: .normal)
            
        } else if currentLanguage == "en" {
            titleFirstItem.text = "Enter home phone number"
            subtitleFirstItem.text = " Enter home phone number "
            numbericLabelFirstItem.text = "12123456"
            textLabelFirstItem.text = ""
            imageFirstItem.alpha = 1.0
            
            titleSecondItem.text = "Enter the amount to pay"
            subtitleSecondItem.text = " Amount "
            numbericLabelSecondItem.text = "15"
            textLabelSecondItem.text = ""
            imageSecondItem.alpha = 1.0
            
            titleThirdItem.text = "Select a bank card for payment"
            subtitleThirdItem.text = " Select a card "
            numbericLabelThirdItem.text = "**** **** **** 4226"
            textLabelThirdItem.text = "You can add a new card in :"
            imageThirdItem.alpha = 1.0
            buttonThirdItem.alpha =  1.0
            buttonThirdItem.setTitle("<<My Cards>>", for: .normal)
            
        } else if currentLanguage == "ru" {
            titleFirstItem.text = "Введите номер домашнего телефона"
            subtitleFirstItem.text = " Введите номер дом.телефона "
            numbericLabelFirstItem.text = "65 260720"
            textLabelFirstItem.text = ""
            imageFirstItem.alpha = 1.0
            
            titleSecondItem.text = "Укажите сумму оплаты"
            subtitleSecondItem.text = " Количество "
            numbericLabelSecondItem.text = "15"
            textLabelSecondItem.text = ""
            imageSecondItem.alpha = 1.0
                      
            titleThirdItem.text = "Выберите банковскую карту для оплаты"
            subtitleThirdItem.text = " Выберите карту "
            numbericLabelThirdItem.text = "**** **** **** 4226"
            textLabelThirdItem.text = "Вы можете добавить банковскую карту на вкладке :"
            imageThirdItem.alpha = 1.0
            buttonThirdItem.alpha =  1.0
            buttonThirdItem.setTitle("<<Мои карты>>", for: .normal)
        }
      
    }
}

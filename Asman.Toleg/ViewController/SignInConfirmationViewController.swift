//
//  SignInConfirmationViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 14.01.2022.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields
import Alamofire

class SignInConfirmationViewController: UIViewController {

    let utils = Utils()
    var downTime = 12
    var newCodeLabelGesture = UITapGestureRecognizer()
    var time = ""
    let LANGUAGE_KEY = "CurrentLanguage"
    let defaults = UserDefaults.standard
    var currentLanguage = "ru"
    var phoneNumber = ""
    var timer = Timer()
    weak var activeField : UITextField?
    let progessView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    @IBOutlet weak var smsCodeTextField: MDCOutlinedTextField!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var contentViewSafeArea: UIView!
    @IBOutlet weak var newCode: UILabel!
    @IBOutlet weak var codeToNumber: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        smsCodeTextField.delegate = self
        
        customizeUI()
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerSelector(_:)), userInfo: nil, repeats: true)
        
        // TODO: @Copy-pasta
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        smsCodeTextField.becomeFirstResponder()
    }
    
    @IBAction func continueButtonAction(_ sender: Any) {
        if utils.validateNumberRange(view: self.smsCodeTextField, min: 5, max: 5) {
            verifyOtpUser()
        } else {
            smsCodeTextField.errorStyle()
        }
    }
    @IBAction func goBack(_ sender: Any) {
        timer.invalidate()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func timerSelector(_ sender : Timer) {
        newCode.textColor = .label
        time = timeString(time: TimeInterval(downTime))
        print(time)
        if currentLanguage == "tk" {
            newCode.text = "Tazeden ibermek üçin \(time)"
        }
        if currentLanguage == "en" {
            newCode.text = "Tazeden ibermek üçin \(time)"
        }
        if currentLanguage == "ru" {
            newCode.text = "Tazeden ibermek üçin \(time)"
        }
        downTime -= 1
        if downTime == -1 {
            sender.invalidate()
            if currentLanguage == "tk" {
                newCode.text = "Kod gelmedi.Tazeden iber."
            }
            if currentLanguage == "en" {
                newCode.text = "Kod gelmedi.Tazeden iber."
            }
            if currentLanguage == "ru" {
                newCode.text = "Kod gelmedi.Tazeden iber."
            }
            newCode.isUserInteractionEnabled = true
            newCode.textColor = .systemBlue
        }

    }
    
    @objc func newCodeLabelTapSelector(_ sender : UITapGestureRecognizer) {
        print("Click")
        loginUser()
    }
    
    //keyboard
    @objc func keyboardDidShow(notification: Notification) {
        print("show keyboard")
        let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        guard let activeField = activeField, var keyboardHeight = keyboardSize?.height else { return }
           
        let tempKeyboardAdditional : CGFloat = 0.0
        let tempMoveAdditional : CGFloat = 0.0
           
        keyboardHeight += tempKeyboardAdditional  // TODO: Temporary hack. Find real value
        let moveViewFromBottomSize = keyboardHeight + tempMoveAdditional
           
           
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: moveViewFromBottomSize, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        let activeRect = activeField.convert(activeField.bounds, to: scrollView)
        scrollView.scrollRectToVisible(activeRect, animated: true)
    }

    @objc func keyboardWillBeHidden(notification: Notification) {
        print("Hide keyboard")
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
       
    @objc func DismissKeyboard(){
        view.endEditing(true)
    }

    
    private  func timeString(time: TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }

   private func customizeUI(){
       continueButton.layer.cornerRadius = 20
   
       newCodeLabelGesture = UITapGestureRecognizer(target: self, action: #selector(newCodeLabelTapSelector(_:)))
       newCode.isUserInteractionEnabled = false
       newCode.addGestureRecognizer(newCodeLabelGesture)
       
       if currentLanguage == "tk" {
           codeToNumber.text = "Tassyklaýyş kody +993\(phoneNumber) belgisine sms arkaly ugradyldy"
       }
       if currentLanguage == "en" {
           codeToNumber.text = ""
       }
       if currentLanguage == "ru" {
           codeToNumber.text = "Мы отправили код подтверждения на номер +993\(phoneNumber)"
       }
    
       
       if utils.getLanguage() == "tk" {
           smsCodeTextField.label.text = "Sms kod"
           smsCodeTextField.leadingAssistiveLabel.text = "Jemi 5 san bolmaly"
           
       } else if utils.getLanguage() == "en" {
           smsCodeTextField.label.text = "Sms code"
           smsCodeTextField.leadingAssistiveLabel.text = "5 digits total"
           
       } else if utils.getLanguage() == "ru" {
           smsCodeTextField.label.text = "Смс код"
           smsCodeTextField.leadingAssistiveLabel.text = "Введите все 5 цифр"
  
       }
       smsCodeTextField.defaultStyle()
       smsCodeTextField.keyboardType = .numberPad
       smsCodeTextField.clearButtonMode = .whileEditing

   }
    
    private func finishPost (message:String, data:Data?) -> Void {
        do {
            if let jsonData = data {
                let parsedData = try JSONDecoder().decode(VerifyOTPResponse.self, from: jsonData)
                print(parsedData)
            }
        }
        catch {
            print("Parse Error: \(error)")
        }
    }
    
    private func getPostString(params:[String:Any]) -> String {
        var data = [String]()
        for(key, value) in params {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
        
    }
    
    private func createActivityIndicator(){
        progessView.backgroundColor = .white
        contentView.addSubview(progessView)
        progessView.translatesAutoresizingMaskIntoConstraints = false
        progessView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        progessView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        progessView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        progessView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        
        activityIndicator = UIActivityIndicatorView(style: .medium)
        contentView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        activityIndicator.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        activityIndicator.startAnimating()
        
    }
    
    private func loginUser() {
        guard let url = URL(string: URLString.URL_LOGIN_USER) else {
         print("Error: cannot create URL")
         return
         }
        
        guard let phoneNumber = Int(phoneNumber) else { print("Error: phone number"); return }
        
         // Add data to the model
         let uploadDataModel = LoginRequest(phone: phoneNumber)
         
         // Convert model to JSON data
         guard let jsonData = try? JSONEncoder().encode(uploadDataModel) else {
             print("Error: Trying to convert model to JSON data")
             return
         }
         
         // Create the url request
         var request = URLRequest(url: url)
         request.httpMethod = "POST"
         request.setValue("application/json", forHTTPHeaderField: "Content-Type") // the request is JSON
         request.setValue("application/json", forHTTPHeaderField: "Accept") // the response expected to be in JSON format
         request.httpBody = jsonData
        createActivityIndicator()
         URLSession.shared.dataTask(with: request) { data, response, error in
             guard error == nil else {
                 DispatchQueue.main.async {
                     self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                     self.activityIndicator.stopAnimating()
                     self.progessView.removeFromSuperview()
                 }
                 print("Error: error calling POST")
                 return
             }
             guard let data = data else {
                 DispatchQueue.main.async {
                     self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                     self.activityIndicator.stopAnimating()
                     self.progessView.removeFromSuperview()
                 }
                 print("Error: Did not receive data")
                 return
             }
             guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                 DispatchQueue.main.async {
                     self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                     self.activityIndicator.stopAnimating()
                     self.progessView.removeFromSuperview()
                 }
                 print("Error: HTTP request failed")
                 return
             }
             do {
                 guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                     DispatchQueue.main.async {
                         self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                         self.activityIndicator.stopAnimating()
                         self.progessView.removeFromSuperview()
                     }
                     print("Error: Cannot convert data to JSON object")
                     return
                 }
                 guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                     print("Error: Cannot convert JSON object to Pretty JSON data")
                     DispatchQueue.main.async {
                         self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                         self.activityIndicator.stopAnimating()
                         self.progessView.removeFromSuperview()
                     }
                     return
                 }
                 guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                     DispatchQueue.main.async {
                         self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                         self.activityIndicator.stopAnimating()
                         self.progessView.removeFromSuperview()
                     }
                     print("Error: Couldn't print JSON in String")
                     return
                 }
                     
                 print(prettyPrintedJson)
                 let decoder = JSONDecoder()
                 let log = try decoder.decode(LoginResponse.self, from: prettyJsonData)
                 if log.success {
                     DispatchQueue.main.async {
                         self.downTime = 120
                         self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerSelector(_:)), userInfo: nil, repeats: true)
                         self.newCode.isUserInteractionEnabled = false
                         
                         self.activityIndicator.stopAnimating()
                         self.progessView.removeFromSuperview()
                     }
                     
                 } else {
                     DispatchQueue.main.async {
                         self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                         self.activityIndicator.stopAnimating()
                         self.progessView.removeFromSuperview()
                     }
                 }
             } catch {
                 DispatchQueue.main.async {
                     self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                     self.activityIndicator.stopAnimating()
                     self.progessView.removeFromSuperview()
                 }
                 print("Error: Trying to convert JSON data to string")
                 return
             }
         }.resume()
         
     }
    
    private func verifyOtpUser() {
        guard let smsCodeString = smsCodeTextField.text else { print("Error: create smsCodeString variable"); return }
        guard let smsCode = Int(smsCodeString) else { print("Error: create smsCode variable"); return }
        createActivityIndicator()
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append("\(smsCode)".data(using: .utf8)!, withName: "otp")
            multipartFormData.append("\(self.phoneNumber)".data(using: .utf8)!, withName: "phone")
        }, to: URL(string: "http://217.174.231.218:9012/api/v1/verify-otp")!) { encodingResult
            in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response.result.value as Any)
                    self.activityIndicator.stopAnimating()
                    self.progessView.removeFromSuperview()
               
                        guard let newsResponse = response.result.value as? [String:Any] else {
                                 print("Error: \(response.result.error)")
                            self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))

                                 return
                             }
                        guard let data = newsResponse["data"] as? [String : Any] else { print("Error: create variable data"); return }
                        
                        guard let accessToken = data["access_token"] else { print("Error: create variable accessToken"); return }
                        
                        print(accessToken)
                        self.defaults.set(accessToken, forKey: "access_token")
                        
                        //presented TabBarController
                        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "tabBarId") as? UITabBarController
                        Theme.defaultTheme()
                        UserDefaults.standard.set(false, forKey: "theme")
                        self.timer.invalidate()
                        self.view.window?.rootViewController = homeViewController
                        self.view.window?.makeKeyAndVisible()

                }
            case .failure(let encodingError):
                print(encodingError)
                self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
    
            }
        }
    }
}
extension SignInConfirmationViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == smsCodeTextField {
            smsCodeTextField.editingStyle()
        }
        activeField = textField

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == smsCodeTextField {
            smsCodeTextField.defaultStyle()
        }
        activeField = nil

    }    
 
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text{
            let invalidCharacters =
                CharacterSet ( charactersIn: "1234567890" ) .inverted
      
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
        if  newString.count <= 5 {
 
        if textField == smsCodeTextField {
            smsCodeTextField.editingStyle()
            return ( string.rangeOfCharacter (from: invalidCharacters) == nil )
            }
        }
            
        }
       return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        smsCodeTextField.defaultStyle()
        return true
    }
}

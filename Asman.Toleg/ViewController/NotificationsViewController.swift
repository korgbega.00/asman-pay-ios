//
//  NotificationsViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 20.01.2022.
//

import UIKit

class NotificationsViewController: UIViewController {

    let utils = Utils()
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizingUI()

        utils.roundCorners(view: toolbarView, radius: 20)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    private func customizingUI() {
        self.toolbarView.backgroundColor = Theme.backgroundColorLight
        self.view.backgroundColor = Theme.backgroundColorLight
        self.scrollContentview.backgroundColor = Theme.backgroundColor
        self.backButton.tintColor = Theme.textColor
        
        //shadow
        utils.setBottomShadow(toolbarView)
    }
}

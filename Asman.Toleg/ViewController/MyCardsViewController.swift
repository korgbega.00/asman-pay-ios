//
//  MyCardsViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 17.01.2022.
//

import UIKit

class MyCardsViewController: UIViewController{
    
    var index : Int?
    var cards : [Card] = []
    var button = UIButton()
    var x = CGFloat()
    var y = CGFloat()
    var bool = true
    var utils = Utils()
    let LANGUAGE_KEY = "CurrentLanguage"
    let defaults = UserDefaults.standard
    var currentLanguage = "ru"
  
    
    @IBOutlet weak var cardsTableView: UITableView!
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var safeAreaContentView: UIView!
    @IBOutlet weak var addCardButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customizingUI()
        
        utils.roundCorners(view: toolbarView, radius: 20)
        
        if let data = UserDefaults.standard.data(forKey: "cards") {
            do {
                let decoder = JSONDecoder()
                let card = try decoder.decode([Card].self, from: data)
                cards = card
                
            } catch {
                print(error)
            }
        }
        cardsTableView.showsVerticalScrollIndicator = false
        cardsTableView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
        cardsTableView.delegate = self
        cardsTableView.dataSource = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tableViewTapped(recognizer:)))
        cardsTableView.addGestureRecognizer(tapGestureRecognizer)

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: NewCardViewController = storyboard.instantiateViewController(withIdentifier: "NewCardViewControllerID") as! NewCardViewController
        vc.delegate = self
        present(vc, animated: true, completion: nil)
 
    }
    
    @objc func buttonAction(sender : UIButton) {
        self.bool = true
        self.button.removeFromSuperview()
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var removeHistoryAlertController : RemoveHistoryAlertController!
        
        removeHistoryAlertController = storyboard.instantiateViewController(withIdentifier: "DeleteHistoryViewControllerID") as? RemoveHistoryAlertController
        removeHistoryAlertController.delegate = self
        let language =  defaults.string(forKey: LANGUAGE_KEY)
        if language == "tk" {
            removeHistoryAlertController.messageString = "Bu karty hakykatdan hem pozmak isleyärmisiñiz?"
        }
        if language == "en" {
            removeHistoryAlertController.messageString = "Are you sure you want to delete this card?"
        }
        if language == "ru" {
            removeHistoryAlertController.messageString = "Вы уверены, что хотите удалить эту карту?"
        }
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alertController.setValue(removeHistoryAlertController, forKey: "contentViewController")
        self.present(alertController, animated: true, completion: nil)
        
     }
    
    @objc func continueButtonAction(sender: UIButton) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: NewCardViewController = storyboard.instantiateViewController(withIdentifier: "NewCardViewControllerID") as! NewCardViewController
        vc.delegate = self
        present(vc, animated: true, completion: nil)

    }
    
    @objc func tableViewTapped(recognizer: UITapGestureRecognizer) {
        let location = recognizer.location(in: self.cardsTableView) // point of touch in tableView
        if let indexPath = self.cardsTableView.indexPathForRow(at: location) { // indexPath of touch location
            if let cell = cardsTableView.cellForRow(at: indexPath) as? CardsTableViewCell {
                let locationInCell = recognizer.location(in: cell) // point of touch in cell
                // do something with location or locationInCell

                if cell.contentView.frame.contains(location) || cell.contentView.frame.contains(locationInCell) {
                    if bool {
                        self.x = location.x
                        self.y = location.y
                        if location.x > cell.center.x { self.x = location.x - cell.center.x }
                        if location.y < cell.center.y { self.y = location.y + 10.0 }
                        if location.y > cell.center.y { self.y = location.y - 40.0 }
                       
                        if let  language = defaults.string(forKey: LANGUAGE_KEY) {
                            currentLanguage = language
                        }
                        if currentLanguage == "tk" {
                            self.button = showMenu(point: CGPoint(x: self.x, y: self.y), contentView: self.cardsTableView, title: " Karty poz" )
                        }
                        if currentLanguage == "en" {
                            self.button = showMenu(point: CGPoint(x: self.x, y: self.y), contentView: self.cardsTableView, title: " Delete card" )
                        }
                        if currentLanguage == "ru" {
                            self.button = showMenu(point: CGPoint(x: self.x, y: self.y), contentView: self.cardsTableView, title: " Удалить карту" )
                        }

                        self.cardsTableView.addSubview(button)
                        self.index = indexPath.row
                        button.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
                        self.bool = false } else {
                            self.button.removeFromSuperview()
                            self.bool = true
                        }
                }
            }
        }
    }
    
    private func customizingUI(){
        self.view.backgroundColor = Theme.backgroundColorLight
        self.safeAreaContentView.backgroundColor = Theme.backgroundColor
        self.toolbarView.backgroundColor = Theme.backgroundColorLight
        self.cardsTableView.backgroundColor = Theme.backgroundColor
        self.addCardButton.tintColor = Theme.textColor
        
        //shadow
        utils.setBottomShadow(toolbarView)
        
    }
}

protocol MyCardsViewControllerDelegate: AnyObject {
    func reloadDataTableView()
    func deleteCardToIndex()
}

extension MyCardsViewController : UITableViewDataSource, UITableViewDelegate, MyCardsViewControllerDelegate {
    
    //MARK: MyCardsViewControllerDelegate
    func deleteCardToIndex() {
        guard let index = index else { return }
        
        self.cards.remove(at: index)
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(self.cards)
            UserDefaults.standard.set(data, forKey: "cards")
        } catch {
            print(error)
        }
        if let  language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }

        if utils.getLanguage() == "tk" {
            showToast(message: "Kart pozuldy", font: .systemFont(ofSize: 12.0))
        }
        if utils.getLanguage() == "en" {
            showToast(message: "Card removed", font: .systemFont(ofSize: 12.0))
        }
        if utils.getLanguage() == "ru" {
            showToast(message: "Карта удалена", font: .systemFont(ofSize: 12.0))
        }
    }
    
   
    func reloadDataTableView() {
        if let data = UserDefaults.standard.data(forKey: "cards") {
            do {
                let decoder = JSONDecoder()
                let card = try decoder.decode([Card].self, from: data)
                self.cards = card
                
            } catch {
                print(error)
            }
        }
        self.cardsTableView.reloadData()
    }
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cards.count == 0 {
            return 1
        }
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.systemGray6

        if cards.count == 0 {
            let cell = cardsTableView.dequeueReusableCell(withIdentifier: "CardsBlankCellID", for: indexPath) as! CardsBlankCell
            cell.selectedBackgroundView = bgColorView
            cell.continueButton.addTarget(self, action: #selector(continueButtonAction(sender:)), for: .touchUpInside)
            return cell

        } else {
            let cell = cardsTableView.dequeueReusableCell(withIdentifier: "IDCardsCell", for: indexPath) as! CardsTableViewCell
            cell.selectedBackgroundView = bgColorView
            cell.numberCardLabel.text = "**** **** **** \(cards[indexPath.row].title)"
            cell.nameLabel.text = cards[indexPath.row].nameLastName
            cell.dateCardsLabel.text = cards[indexPath.row].cardDate
            
            return cell }
        
    }
    
     //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if cards.count == 0 {
            return 182
        } else {
            return 89
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.index = indexPath.row
    }
    
}

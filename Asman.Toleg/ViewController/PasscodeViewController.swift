//
//  PasscodeViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 11.02.2022.
//

import UIKit
import AudioToolbox


class PasscodeViewController: UIViewController {

    var animation = CABasicAnimation()
    var appCode : String?
    var errorCount = 1
    var openTabBar = true
    
    var oneTouchDown = UILongPressGestureRecognizer()
    var twoTouchDown = UILongPressGestureRecognizer()
    var threeTouchDown = UILongPressGestureRecognizer()
    var fourTouchDown = UILongPressGestureRecognizer()
    var fiveTouchDown = UILongPressGestureRecognizer()
    var sixTouchDown = UILongPressGestureRecognizer()
    var sevenTouchDown = UILongPressGestureRecognizer()
    var eigthTouchDown = UILongPressGestureRecognizer()
    var nineTouchDown = UILongPressGestureRecognizer()
    var zeroTouchDown = UILongPressGestureRecognizer()
    var deleteTouchDown = UILongPressGestureRecognizer()

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var one: UIView!
    @IBOutlet weak var two: UIView!
    @IBOutlet weak var three: UIView!
    @IBOutlet weak var four: UIView!
    @IBOutlet weak var five: UIView!
    @IBOutlet weak var six: UIView!
    @IBOutlet weak var seven: UIView!
    @IBOutlet weak var eigth: UIView!
    @IBOutlet weak var nine: UIView!
    @IBOutlet weak var zero: UIView!
    @IBOutlet weak var delete: UIView!
    
    @IBOutlet weak var leftConstraintStack_1: NSLayoutConstraint!
    @IBOutlet weak var leftConstraintStack_2: NSLayoutConstraint!
    @IBOutlet weak var leftConstraintStack_3: NSLayoutConstraint!
    @IBOutlet weak var leftConstraintStack_4: NSLayoutConstraint!
    @IBOutlet weak var rightConstraintStack_1: NSLayoutConstraint!
    @IBOutlet weak var rightConstraintStack_2: NSLayoutConstraint!
    @IBOutlet weak var rightConstraintStack_3: NSLayoutConstraint!
    @IBOutlet weak var rightConstraintStack_4: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var oneLabel: UILabel!
    @IBOutlet weak var twoLabel: UILabel!
    @IBOutlet weak var threeLabel: UILabel!
    @IBOutlet weak var fourLabel: UILabel!
    @IBOutlet weak var fiveLabel: UILabel!
    @IBOutlet weak var sixLabel: UILabel!
    @IBOutlet weak var sevenLabel: UILabel!
    @IBOutlet weak var eightLabel: UILabel!
    @IBOutlet weak var nineLabel: UILabel!
    @IBOutlet weak var zeroLabel: UILabel!
    @IBOutlet weak var abcLabel: UILabel!
    @IBOutlet weak var defLabel: UILabel!
    @IBOutlet weak var ghiLabel: UILabel!
    @IBOutlet weak var jklLabel: UILabel!
    @IBOutlet weak var mnoLabel: UILabel!
    @IBOutlet weak var pqrsLabel: UILabel!
    @IBOutlet weak var tuvLabel: UILabel!
    @IBOutlet weak var wxyzLabel: UILabel!
    @IBOutlet weak var addlabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textField.delegate = self
        
        customizingUI()
        
        appCode = UserDefaults.standard.string(forKey: "appCode")
        print(appCode as Any)
        
        errorCount = UserDefaults.standard.integer(forKey: "errorCount")
        print("errorCount = \(errorCount)")
        
        //MARK: shake animation
        animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 1
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.view.center.x + 10, y: label.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.view.center.x - 10, y: label.center.y))
        
        leftConstraintStack_1.constant = (self.view.frame.width - one.frame.width * 3)/4
        rightConstraintStack_1.constant = (self.view.frame.width  - one.frame.width * 3)/4
     
        leftConstraintStack_2.constant = (self.view.frame.width - one.frame.width * 3)/4
        rightConstraintStack_2.constant = (self.view.frame.width  - one.frame.width * 3)/4
     
        leftConstraintStack_3.constant = (self.view.frame.width - one.frame.width * 3)/4
        rightConstraintStack_3.constant = (self.view.frame.width  - one.frame.width * 3)/4
     
        leftConstraintStack_4.constant = (self.view.frame.width - one.frame.width * 3)/4
        rightConstraintStack_4.constant = (self.view.frame.width  - one.frame.width * 3)/4
     
        one.layer.cornerRadius = one.frame.width/2
        two.layer.cornerRadius = two.frame.width/2
        three.layer.cornerRadius = three.frame.width/2
        four.layer.cornerRadius = four.frame.width/2
        five.layer.cornerRadius = five.frame.width/2
        six.layer.cornerRadius = six.frame.width/2
        seven.layer.cornerRadius = seven.frame.width/2
        eigth.layer.cornerRadius = eigth.frame.width/2
        nine.layer.cornerRadius = nine.frame.width/2
        zero.layer.cornerRadius = zero.frame.width/2
        delete.layer.cornerRadius = delete.frame.width/2

        setupTap()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
    
    func setupTap() {
        //MARK: one
        oneTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(didTouchDown))
        oneTouchDown.minimumPressDuration = 0
        one.addGestureRecognizer(oneTouchDown)
        
        //MARK: two
        twoTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(didTouchDown))
        twoTouchDown.minimumPressDuration = 0
        two.addGestureRecognizer(twoTouchDown)
        
        //MARK: three
        threeTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(didTouchDown))
        threeTouchDown.minimumPressDuration = 0
        three.addGestureRecognizer(threeTouchDown)

        //MARK: four
        fourTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(didTouchDown))
        fourTouchDown.minimumPressDuration = 0
        four.addGestureRecognizer(fourTouchDown)

        //MARK: five
        fiveTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(didTouchDown))
        fiveTouchDown.minimumPressDuration = 0
        five.addGestureRecognizer(fiveTouchDown)

        //MARK: six
        sixTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(didTouchDown))
        sixTouchDown.minimumPressDuration = 0
        six.addGestureRecognizer(sixTouchDown)

        //MARK: seven
        sevenTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(didTouchDown))
        sevenTouchDown.minimumPressDuration = 0
        seven.addGestureRecognizer(sevenTouchDown)

        //MARK: eigth
        eigthTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(didTouchDown))
        eigthTouchDown.minimumPressDuration = 0
        eigth.addGestureRecognizer(eigthTouchDown)

        //MARK: nine
        nineTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(didTouchDown))
        nineTouchDown.minimumPressDuration = 0
        nine.addGestureRecognizer(nineTouchDown)

        //MARK: zero
        zeroTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(didTouchDown))
        zeroTouchDown.minimumPressDuration = 0
        zero.addGestureRecognizer(zeroTouchDown)

        //MARK: delete
        deleteTouchDown = UILongPressGestureRecognizer(target:self, action: #selector(didTouchDown))
        deleteTouchDown.minimumPressDuration = 0
        delete.addGestureRecognizer(deleteTouchDown)

  
    }

    @objc func didTouchDown(gesture: UILongPressGestureRecognizer) {
        guard var text = textField.text else {return}
        
        //MARK: delete
        if gesture == deleteTouchDown {
            if gesture.state == .began {
                delete.backgroundColor = UIColor(cgColor: CGColor(red: CGFloat(0x32)/255, green: CGFloat(0x99)/255, blue: CGFloat(0xE1)/255, alpha: 1))
                
            } else if gesture.state == .ended || gesture.state == .cancelled {
                delete.backgroundColor = UIColor(white: 1, alpha: 0)
                if text == "" { return }
                text.removeLast()
                textField.text = "\(text)"
                print(textField.text as Any)

            }
        }
        
        if text.count >= 4 {return}
        
        //MARK: One
        if gesture == oneTouchDown {
            touchDown(number: 1, text: text, gesture: gesture, view: one)
        }
        //MARK: two
        if gesture == twoTouchDown {
            touchDown(number: 2, text: text, gesture: gesture, view: two)
        }
        
        //MARK: three
        if gesture == threeTouchDown {
            touchDown(number: 3, text: text, gesture: gesture, view: three)
        }
        
        //MARK: four
        if gesture == fourTouchDown {
            touchDown(number: 4, text: text, gesture: gesture, view: four)
        }
        
        //MARK: five
        if gesture == fiveTouchDown {
            touchDown(number: 5, text: text, gesture: gesture, view: five)
        }
        
        //MARK: six
        if gesture == sixTouchDown {
            touchDown(number: 6, text: text, gesture: gesture, view: six)
        }
        
        //MARK: seven
        if gesture == sevenTouchDown {
            touchDown(number: 7, text: text, gesture: gesture, view: seven)
        }
        
        //MARK: eight
        if gesture == eigthTouchDown {
            touchDown(number: 8, text: text, gesture: gesture, view: eigth)
        }
        
        //MARK: nine
        if gesture == nineTouchDown {
            touchDown(number: 9, text: text, gesture: gesture, view: nine)
        }
        
        //MARK: zero
        if gesture == zeroTouchDown {
            touchDown(number: 0, text: text, gesture: gesture, view: zero)
        }
       
    }
    
    func touchDown(number : Int, text : String, gesture : UILongPressGestureRecognizer, view : UIView){
        if gesture.state == .began {
            view.backgroundColor = UIColor(cgColor: CGColor(red: CGFloat(0x32)/255, green: CGFloat(0x99)/255, blue: CGFloat(0xE1)/255, alpha: 1))
            
        } else if gesture.state == .ended || gesture.state == .cancelled {
            view.backgroundColor = UIColor(white: 1, alpha: 0)
            textField.text = "\(text)\(number)"
            print(textField.text as Any)
            if textField.text!.count == 4 {
                nextTabBarController()
            }
        }
    }
    
    func nextTabBarController() {
        if textField.text == appCode {
            if openTabBar {
                UserDefaults.standard.removeObject(forKey: "errorCount")
                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "tabBarId") as? UITabBarController
                self.view.window?.rootViewController = homeViewController
                self.view.window?.makeKeyAndVisible()

            } else {
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            if errorCount >= 2 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PasscodeTimerViewControllerID") as? PasscodeTimerViewController
                vc?.modalPresentationStyle = .fullScreen
                present(vc!, animated: false, completion: nil)
                UserDefaults.standard.set( errorCount, forKey: "errorCount")
            }
            errorCount += 1
            label.layer.add(animation, forKey: "position")
            //MARK: VIBRATE IPHONE
            UINotificationFeedbackGenerator().notificationOccurred(.error)
            textField.text = ""
            
        }
    }
    
    private func customizingUI() {
        self.view.backgroundColor = Theme.backgroundColorLight
        titleLabel.textColor = .white
        zeroLabel.textColor = .white
        oneLabel.textColor = .white
        twoLabel.textColor = .white
        threeLabel.textColor = .white
        fourLabel.textColor = .white
        fiveLabel.textColor = .white
        sixLabel.textColor = .white
        sevenLabel.textColor = .white
        eightLabel.textColor = .white
        nineLabel.textColor = .white
        abcLabel.textColor = .white
        defLabel.textColor = .white
        ghiLabel.textColor = .white
        jklLabel.textColor = .white
        mnoLabel.textColor = .white
        pqrsLabel.textColor = .white
        tuvLabel.textColor = .white
        wxyzLabel.textColor = .white
        addlabel.textColor = .white

    }
}
extension PasscodeViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text{
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            if  newString.count <= 4 { return true }
         }
        
       return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

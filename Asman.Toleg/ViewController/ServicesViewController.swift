//
//  ViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 20.12.2021.
//

import UIKit
import MaterialComponents.MaterialButtons

class ServicesViewController: UIViewController {

    private let utils = Utils()
    private var conOffset = CGPoint()
    @IBOutlet weak var floatingButton: FloatingButton!
    @IBOutlet weak var heightFloatingButton: NSLayoutConstraint!
    @IBOutlet weak var widthFoatingButton: NSLayoutConstraint!
    @IBOutlet weak var rightFloatingButton: NSLayoutConstraint!
    @IBOutlet weak var bottomFloatingButton: NSLayoutConstraint!
    
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var roundView: UIView!
    
    @IBOutlet weak var pyggImageView: UIImageView!
    @IBOutlet weak var tmCellImageView: UIImageView!
    @IBOutlet weak var telInternetImageView: UIImageView!
    @IBOutlet weak var ASTUInternetImageView: UIImageView!
    @IBOutlet weak var ASTUTelephoneImageView: UIImageView!
    @IBOutlet weak var ASTUIPTVImageView: UIImageView!
    
    @IBOutlet weak var tapPYGGView: UIView!
    @IBOutlet weak var tapTMCellView: UIView!
    @IBOutlet weak var tapTelInternetView: UIView!
    @IBOutlet weak var tapASTUInternetView: UIView!
    @IBOutlet weak var tapASTUTelephoneView: UIView!
    @IBOutlet weak var tapASTUIPTVView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var safeAreaContentView: UIView!
    @IBOutlet weak var lineView1: UIView!
    @IBOutlet weak var lineView2: UIView!
    @IBOutlet weak var lineView3: UIView!
    @IBOutlet weak var lineView4: UIView!
    @IBOutlet weak var lineView5: UIView!
    @IBOutlet weak var notificationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        customizingUI()
        
        utils.roundCorners(view: toolbarView, radius: 20)
        roundView.layer.cornerRadius = 20
        pyggImageView.layer.cornerRadius = 10
        tmCellImageView.layer.cornerRadius = 10
        telInternetImageView.layer.cornerRadius = 10
        ASTUInternetImageView.layer.cornerRadius = 10
        ASTUTelephoneImageView.layer.cornerRadius = 10
        ASTUIPTVImageView.layer.cornerRadius = 10
          
        let gesturePygg = UITapGestureRecognizer(target: self, action:  #selector(self.openPyggPaymentAction))
        let gestureTMCell = UITapGestureRecognizer(target: self, action:  #selector(self.openTMCellPaymentAction))
        let gestureTelInternet = UITapGestureRecognizer(target: self, action:  #selector(self.openTelInternetPaymentAction))
        let gestureASTUInternet = UITapGestureRecognizer(target: self, action:  #selector(self.openASTUInternetPaymentAction))
        let gestureASTUTelephone = UITapGestureRecognizer(target: self, action:  #selector(self.openASTUTelephonePaymentAction))
        let gestureASTUIPTV = UITapGestureRecognizer(target: self, action:  #selector(self.openASTUIPTVPaymentAction))
        let gestureFloatingButton = UITapGestureRecognizer(target: self, action:  #selector(self.flotingButtonAction))
      
        
        self.tapPYGGView.addGestureRecognizer(gesturePygg)
        self.tapTMCellView.addGestureRecognizer(gestureTMCell)
        self.tapTelInternetView.addGestureRecognizer(gestureTelInternet)
        self.tapASTUInternetView.addGestureRecognizer(gestureASTUInternet)
        self.tapASTUTelephoneView.addGestureRecognizer(gestureASTUTelephone)
        self.tapASTUIPTVView.addGestureRecognizer(gestureASTUIPTV)
        
        self.floatingButton.addGestureRecognizer(gestureFloatingButton)
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
    
    @IBAction func openNotificationsAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var vc = NotificationsViewController()
        vc = storyboard.instantiateViewController(withIdentifier: "NotificationsViewControllerID") as! NotificationsViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func openPyggPaymentAction(sender : UITapGestureRecognizer) {
        performSegue(withIdentifier: "segueShowPYGGController", sender: sender)
    }
  
    @objc func openTMCellPaymentAction(sender : UITapGestureRecognizer) {
        performSegue(withIdentifier: "segueShowTMCellController", sender: sender)
    }
  
    @objc func openTelInternetPaymentAction(sender : UITapGestureRecognizer) {
        performSegue(withIdentifier: "segueShowTelecomInternetController", sender: sender)
    }
 
    @objc func openASTUInternetPaymentAction(sender : UITapGestureRecognizer) {
        performSegue(withIdentifier: "segueShowASTUInternetController", sender: sender)
    }
 
    @objc func openASTUTelephonePaymentAction(sender : UITapGestureRecognizer) {
        performSegue(withIdentifier: "segueShowASTUPhoneController", sender: sender)
    }
 
    @objc func openASTUIPTVPaymentAction(sender : UITapGestureRecognizer) {
        performSegue(withIdentifier: "segueShowASTUIptvController", sender: sender)
    }
    
    @objc func flotingButtonAction(sender : UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var vc = ContactViewController()
        vc = storyboard.instantiateViewController(withIdentifier: "ContactViewControllerID") as! ContactViewController
        present(vc, animated: true, completion: nil)
    
    }
    private func customizingUI(){
        scrollContentView.backgroundColor = Theme.backgroundColor
        scrollView.backgroundColor = Theme.backgroundColor
        safeAreaContentView.backgroundColor = Theme.backgroundColor
        pyggImageView.backgroundColor = UIColor.green
        //#9CD6FF
        tmCellImageView.backgroundColor = UIColor(cgColor: CGColor(red: CGFloat(0x9C)/255, green: CGFloat(0xD6)/255, blue: CGFloat(0xFF)/255, alpha: 1))
        //#FFE0AB
        telInternetImageView.backgroundColor = UIColor(cgColor: CGColor(red: CGFloat(0xFF)/255, green: CGFloat(0xE0)/255, blue: CGFloat(0xAB)/255, alpha: 1))
        //#B8B4FF
        ASTUInternetImageView.backgroundColor = UIColor(cgColor: CGColor(red: CGFloat(0xB8)/255, green: CGFloat(0xB4)/255, blue: CGFloat(0xFF)/255, alpha: 1))
        //#B6FFB9
        ASTUTelephoneImageView.backgroundColor = UIColor(cgColor: CGColor(red: CGFloat(0xB6)/255, green: CGFloat(0xFF)/255, blue: CGFloat(0xB9)/255, alpha: 1))
        //#FFC1B7
        ASTUIPTVImageView.backgroundColor = UIColor(cgColor: CGColor(red: CGFloat(0xFF)/255, green: CGFloat(0xC1)/255, blue: CGFloat(0xB9)/255, alpha: 1))
        lineView1.backgroundColor = Theme.lineColor
        lineView2.backgroundColor = Theme.lineColor
        lineView3.backgroundColor = Theme.lineColor
        lineView4.backgroundColor = Theme.lineColor
        lineView5.backgroundColor = Theme.lineColor
        self.view.backgroundColor = Theme.backgroundColorLight
        toolbarView.backgroundColor = Theme.backgroundColorLight
        roundView.backgroundColor = Theme.backgroundColorLight
        tapPYGGView.backgroundColor = Theme.backgroundColorLight
        tapTMCellView.backgroundColor = Theme.backgroundColorLight
        tapASTUIPTVView.backgroundColor = Theme.backgroundColorLight
        tapTelInternetView.backgroundColor = Theme.backgroundColorLight
        tapASTUInternetView.backgroundColor = Theme.backgroundColorLight
        tapASTUTelephoneView.backgroundColor = Theme.backgroundColorLight
        self.tabBarController?.tabBar.backgroundColor = Theme.backgroundColorLight
        self.tabBarController?.tabBar.tintColor = Theme.textColor
        self.tabBarController?.tabBar.unselectedItemTintColor = Theme.tabBarTintColor
        self.notificationButton.tintColor = Theme.iconTintColor
        
        //shadow
        utils.setShadow(roundView)
        utils.setBottomShadow(toolbarView)
        
        
        
    }
}
extension ServicesViewController : UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        conOffset = scrollView.contentOffset

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y > conOffset.y {
            heightFloatingButton.constant = 0
            widthFoatingButton.constant = 0
            rightFloatingButton.constant = 45
            bottomFloatingButton.constant = 45

            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        } else {
            heightFloatingButton.constant = 50
            widthFoatingButton.constant = 50
            rightFloatingButton.constant = 20
            bottomFloatingButton.constant = 20

            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
}



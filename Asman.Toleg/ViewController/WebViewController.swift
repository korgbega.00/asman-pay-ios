//
//  WebViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 10.05.2022.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    let urlString = "https://developer.apple.com/documentation/webkit/wkwebview"
    let aView = UIView()
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var toolBarView: UIView!
    var activityIndicator = UIActivityIndicatorView()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        customizingUI()
        isIndicatorVisibility(true)

        let url = URL(string: urlString)
        let urlRequest = URLRequest(url: url!)
        
        webView.load(urlRequest)
        webView.navigationDelegate = self

    }
    
    @IBAction func goBack(_ sender: Any) {
        if webView.canGoBack {
            webView.goBack()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func isIndicatorVisibility(_ bool: Bool ){
        if bool {
            aView.backgroundColor = Theme.backgroundColor
//            aView.layer.cornerRadius = 10
            webView.addSubview(aView)
            aView.translatesAutoresizingMaskIntoConstraints = false
            aView.topAnchor.constraint(equalTo: webView.topAnchor).isActive = true
            aView.bottomAnchor.constraint(equalTo: webView.bottomAnchor).isActive = true
            aView.leftAnchor.constraint(equalTo: webView.leftAnchor).isActive = true
            aView.rightAnchor.constraint(equalTo: webView.rightAnchor).isActive = true
            
            activityIndicator = UIActivityIndicatorView(style: .medium)
            activityIndicator.color = Theme.textColor
            aView.addSubview(activityIndicator)
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            
            activityIndicator.centerXAnchor.constraint(equalTo: aView.centerXAnchor).isActive = true
            activityIndicator.centerYAnchor.constraint(equalTo: aView.centerYAnchor).isActive = true
            activityIndicator.startAnimating()
        } else {
            aView.removeFromSuperview()
        }
    }
    
    private func customizingUI() {
        self.view.backgroundColor = Theme.backgroundColorLight
        backButton.tintColor = Theme.textColor
        toolBarView.backgroundColor = Theme.backgroundColorLight
        webView.backgroundColor = Theme.backgroundColor
    }
    
}

extension WebViewController : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("start")
        isIndicatorVisibility(true)

    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finish")
        isIndicatorVisibility(false)

    }
}

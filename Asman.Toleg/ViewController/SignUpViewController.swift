//
//  SignUpViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 04.01.2022.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class SignUpViewController: UIViewController {

    let utils = Utils()
    weak var activeField : UITextField?
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var nameTextField: MDCOutlinedTextField!
    @IBOutlet weak var lastnameTextField: MDCOutlinedTextField!
    @IBOutlet weak var phoneNumberTextField: MDCOutlinedTextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameTextField.delegate = self
        lastnameTextField.delegate = self
        phoneNumberTextField.delegate = self

        customizingUI()

        // TODO: @Copy-pasta
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        nameTextField.becomeFirstResponder()
        
    }
    @IBAction func continueButtonAction(_ sender: Any) {
        utils.validateNumberRange(view: nameTextField, min: 1, max: 100)
        utils.validateNumberRange(view: lastnameTextField, min: 1, max: 100)
        utils.validateNumberRange(view: phoneNumberTextField, min: 8, max: 8)
        
        if utils.validateNumberRange(view: nameTextField, min: 1, max: 100) {
            if utils.validateNumberRange(view: lastnameTextField, min: 1, max: 100) {
                if utils.validateNumberRange(view: phoneNumberTextField, min: 8, max: 8){
                    utils.indicatorIsVisibility(indicator: activityIndicator, view: contentView, bool: true)
                    self.showToast(message: "Awtorizasiya boldunyz", font: .systemFont(ofSize: 12.0))
                    
                }
                
            }
            
        }
        
    }
   
    
    @IBAction func goBackAction(_ sender: Any) {
        resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    //keyboard
    @objc func keyboardDidShow(notification: Notification) {
        print("show keyboard")
        let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        guard let activeField = activeField, var keyboardHeight = keyboardSize?.height else { return }
           
        let tempKeyboardAdditional : CGFloat = 0.0
        let tempMoveAdditional : CGFloat = 0.0
           
        keyboardHeight += tempKeyboardAdditional  // TODO: Temporary hack. Find real value
        let moveViewFromBottomSize = keyboardHeight + tempMoveAdditional
           
           
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: moveViewFromBottomSize, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        let activeRect = activeField.convert(activeField.bounds, to: scrollView)
        scrollView.scrollRectToVisible(activeRect, animated: true)
    }

    @objc func keyboardWillBeHidden(notification: Notification) {
        print("Hide keyboard")
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
       
    @objc func DismissKeyboard(){
        view.endEditing(true)
    }
    private func customizingUI() {
        continueButton.layer.cornerRadius = 20

        if utils.getLanguage() == "tk" {
            phoneNumberTextField.label.text = "Telefon belgisi"
            phoneNumberTextField.leadingAssistiveLabel.text = "+993-den galany ýaz"
            nameTextField.label.text = "Ady"
            nameTextField.leadingAssistiveLabel.text = "Adyňyzy giriziň"
            lastnameTextField.label.text = "Familiýasy"
            lastnameTextField.leadingAssistiveLabel.text = "Familiýaňyzy giriziň"

            
        } else if utils.getLanguage() == "en" {
            phoneNumberTextField.label.text = "Phone number"
            phoneNumberTextField.leadingAssistiveLabel.text = "Write without +993"
            nameTextField.label.text = "Name"
            nameTextField.leadingAssistiveLabel.text = "Enter name"
            lastnameTextField.label.text = "Lastname"
            lastnameTextField.leadingAssistiveLabel.text = "Enter lastname"

            
        } else if utils.getLanguage() == "ru" {
            phoneNumberTextField.label.text = "Номер телефона"
            phoneNumberTextField.leadingAssistiveLabel.text = "Пишите без +993"
            nameTextField.label.text = "Имя"
            nameTextField.leadingAssistiveLabel.text = "Введите имя"
            lastnameTextField.label.text = "Фамилия"
            lastnameTextField.leadingAssistiveLabel.text = "Введите фамилию"

        }
        phoneNumberTextField.defaultStyle()
        nameTextField.defaultStyle()
        lastnameTextField.defaultStyle()
        
        phoneNumberTextField.keyboardType = .phonePad
        phoneNumberTextField.clearButtonMode = .whileEditing
        nameTextField.keyboardType = .default
        nameTextField.clearButtonMode = .whileEditing
        lastnameTextField.keyboardType = .default
        lastnameTextField.clearButtonMode = .whileEditing

        
    }

}
extension SignUpViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phoneNumberTextField {
            phoneNumberTextField.editingStyle()
        }
        if textField == nameTextField {
            nameTextField.editingStyle()
        }
        if textField == lastnameTextField {
            lastnameTextField.editingStyle()
        }
        activeField = textField

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == phoneNumberTextField {
            phoneNumberTextField.defaultStyle()
        }
        if textField == nameTextField {
            nameTextField.defaultStyle()
        }
        if textField == lastnameTextField {
            lastnameTextField.defaultStyle()
        }
        activeField = nil

    }
  
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text{
            let invalidCharactersNumbers =
                CharacterSet ( charactersIn: "1234567890" ) .inverted
            let invalidCharacters =
                CharacterSet ( charactersIn: "QWERTYUIOPASDFGHJKLZXCVBNMÜüqwertyuiopasdfghjklzxcvbnmçäöňşý .-," ) .inverted
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
          
            if textField == phoneNumberTextField {
                if  newString.count <= 8 {
 
                    phoneNumberTextField.editingStyle()
            return ( string.rangeOfCharacter (from: invalidCharactersNumbers) == nil )
            }
        }
            
            if textField == nameTextField {
                if  newString.count < 100 {
                    nameTextField.editingStyle()
              return ( string.rangeOfCharacter (from: invalidCharacters) == nil )
              }
          }
            
            if textField == lastnameTextField {
                if  newString.count < 100 {
                    lastnameTextField.editingStyle()
              return ( string.rangeOfCharacter (from: invalidCharacters) == nil )
              }
          }
            
        }
       return false
   
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        phoneNumberTextField.defaultStyle()
        nameTextField.defaultStyle()
        lastnameTextField.defaultStyle()

        return true
    }
}

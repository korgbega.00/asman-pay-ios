//
//  SignInViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 04.01.2022.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class SignInViewController: UIViewController {

    let utils = Utils()
    let whiteImage = UIImage(named: "white-image")
    var checkmarkVisibility = true
    let smallConfig = UIImage.SymbolConfiguration(pointSize: 20, weight: UIImage.SymbolWeight.light , scale: .small)
    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    var termsUseAlertController : TermsUseAlertController!
    let LANGUAGE_KEY = "CurrentLanguage"
    let defaults = UserDefaults.standard
    var currentLanguage = "ru"
    var animation = CABasicAnimation()
    var termText = "Ulanyş düzgünnamasyny kabul edýarin"
    var term = "Ulanyş düzgünnamasyny"
    weak var activeField : UITextField?
    let progessView = UIView()
    var activityIndicator = UIActivityIndicatorView()

    @IBOutlet weak var contentViewSafeArea: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var changeLanguageButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var phoneNumberTextField: MDCOutlinedTextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defaults.set("tk", forKey: LANGUAGE_KEY)
        NotificationCenter.default.post(name: AppNotification.changeLanguage, object: nil)

        phoneNumberTextField.delegate = self
    
        customizingUI()
               
        // TODO: @Copy-pasta
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapView: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        view.addGestureRecognizer(tapView)
   
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        phoneNumberTextField.becomeFirstResponder()
    }
    
    @IBAction func checkboxAction(_ sender: Any) {
        if !checkmarkVisibility {
            checkBoxButton.setImage(whiteImage, for: .normal)
            checkmarkVisibility = true
            checkBoxButton.scalesLargeContentImage = false
            
        } else {
                checkBoxButton.setImage(UIImage(systemName: "checkmark", withConfiguration: smallConfig), for: .normal)
                checkmarkVisibility = false
            }
    }
    
    @IBAction func changeLanguageAction(_ sender: Any) {
        var languageAlertController : LanguageAlertController!
        
        languageAlertController = mainStoryboard.instantiateViewController(withIdentifier: "alertLanguage") as? LanguageAlertController
        languageAlertController.delegate = self

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alertController.setValue(languageAlertController, forKey: "contentViewController")
        self.present(alertController, animated: true, completion: nil)

    }
    
    @IBAction func signUpAction(_ sender: Any) {
        
    }
    
    @IBAction func continueAction(_ sender: Any) {
        guard let phoneString = phoneNumberTextField.text else {return}
        utils.validateNumberRange(view: phoneNumberTextField, min: 8, max: 8)
        if let i = Int(phoneString){
            if i < 60000000  || i > 65999999 {
                phoneNumberTextField.errorStyle()
                if checkmarkVisibility {
               checkBoxButton.layer.add(animation, forKey: "position")
               //MARK: VIBRATE IPHONE
               UINotificationFeedbackGenerator().notificationOccurred(.error)
                }
                return
            }
        }
        if utils.validateNumberRange(view: phoneNumberTextField, min: 8, max: 8) && !checkmarkVisibility {
            loginUser()
        }
               if checkmarkVisibility {
              checkBoxButton.layer.add(animation, forKey: "position")
              //MARK: VIBRATE IPHONE
              UINotificationFeedbackGenerator().notificationOccurred(.error)
        }
    }
    
    @objc func handleTermTapped(gesture: UITapGestureRecognizer) {
        let termString = termText as NSString
        let termRange = termString.range(of: term)

        let tapLocation = gesture.location(in: termsLabel)
        let index = indexOfAttributedTextCharacterAtPoint(point: tapLocation, label: termsLabel)

        if checkRange(termRange, contain: index) == true {
            termsUseAlertController = mainStoryboard.instantiateViewController(withIdentifier: "termsOfUse") as? TermsUseAlertController
            termsUseAlertController.delegate = self
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            alertController.setValue(termsUseAlertController, forKey: "contentViewController")
            self.present(alertController, animated: true, completion: nil)

            return
        }

    }
    
    
    //keyboard
    @objc func keyboardDidShow(notification: Notification) {
        print("show keyboard")
        let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        guard let activeField = activeField, var keyboardHeight = keyboardSize?.height else { return }
           
        let tempKeyboardAdditional : CGFloat = 0.0
        let tempMoveAdditional : CGFloat = 0.0
           
        keyboardHeight += tempKeyboardAdditional  // TODO: Temporary hack. Find real value
        let moveViewFromBottomSize = keyboardHeight + tempMoveAdditional
           
           
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: moveViewFromBottomSize, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        let activeRect = activeField.convert(activeField.bounds, to: scrollView)
        scrollView.scrollRectToVisible(activeRect, animated: true)
    }

    @objc func keyboardWillBeHidden(notification: Notification) {
        print("Hide keyboard")
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
       
    @objc func DismissKeyboard(){
        view.endEditing(true)
    }
    
    private func format(strings: [String],
                   linkFont: UIFont,
                   linkColor: UIColor,
                   inString string: String,
                   font: UIFont,
                   color: UIColor = UIColor.black) -> NSAttributedString {
       
       let attributedString =
           NSMutableAttributedString(string: string,
                                   attributes: [
                                       NSAttributedString.Key.font: font,
                                       NSAttributedString.Key.foregroundColor: color])
       
       let linkFontAttribute = [NSAttributedString.Key.font: linkFont, NSAttributedString.Key.foregroundColor: linkColor,
                                NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue] as [NSAttributedString.Key : Any]
       for link in strings {
           attributedString.addAttributes(linkFontAttribute, range: (string as NSString).range(of: link))
       }
       return attributedString
   }
    private func indexOfAttributedTextCharacterAtPoint(point: CGPoint, label : UILabel) -> Int {
        assert(termsLabel.attributedText != nil, "This method is developed for attributed string")
        let textStorage = NSTextStorage(attributedString: termsLabel.attributedText!)
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: termsLabel.frame.size)
        textContainer.lineFragmentPadding = 0
        textContainer.maximumNumberOfLines = termsLabel.numberOfLines
        textContainer.lineBreakMode = termsLabel.lineBreakMode
        layoutManager.addTextContainer(textContainer)

        let index = layoutManager.characterIndex(for: point, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return index
        
    }
    
    private func checkRange(_ range: NSRange, contain index: Int) -> Bool {
        return index > range.location && index < range.location + range.length
        
    }
    
    private func createActivityIndicator(){
        progessView.backgroundColor = .white
        contentView.addSubview(progessView)
        progessView.translatesAutoresizingMaskIntoConstraints = false
        progessView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        progessView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        progessView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        progessView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        
        activityIndicator = UIActivityIndicatorView(style: .medium)
        contentView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        activityIndicator.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        activityIndicator.startAnimating()
        
    }
    
    private func customizingUI(){
        if utils.getLanguage() == "tk" {
            phoneNumberTextField.label.text = "Telefon belgisi"
            phoneNumberTextField.leadingAssistiveLabel.text = "+993-den galany ýaz"
               
        } else if utils.getLanguage() == "en" {
            phoneNumberTextField.label.text = "Phone number"
            phoneNumberTextField.leadingAssistiveLabel.text = "Write without +993"
            
        } else if utils.getLanguage() == "ru" {
            phoneNumberTextField.label.text = "Номер телефона"
            phoneNumberTextField.leadingAssistiveLabel.text = "Пишите без +993"
        }
        phoneNumberTextField.defaultStyle()
        phoneNumberTextField.keyboardType = .phonePad
        phoneNumberTextField.clearButtonMode = .whileEditing

        
        let imageView = UIImageView(image: UIImage(systemName: "chevron.down"))
        
        contentView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.rightAnchor.constraint(equalTo: changeLanguageButton.rightAnchor, constant: -10).isActive = true
        imageView.centerYAnchor.constraint(equalTo: changeLanguageButton.centerYAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 18).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 15).isActive = true
    
        
        changeLanguageButton.layer.cornerRadius = 15
        continueButton.layer.cornerRadius = 20
        
        checkBoxButton.layer.cornerRadius = 5
        checkBoxButton.layer.borderWidth = 1
        checkBoxButton.layer.borderColor = CGColor(red: 0 , green: 0, blue: 0, alpha: 1)
        
        
        //MARK: shake animation
        animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 1
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: checkBoxButton.center.x + 5, y: checkBoxButton.center.y + 0))
        
        animation.toValue = NSValue(cgPoint: CGPoint(x: checkBoxButton.center.x - 5, y: checkBoxButton.center.y + 0))
        
        //other
        let formattedText = format(strings: [term],
                                          linkFont: UIFont.systemFont(ofSize: 14),
                                          linkColor: UIColor.link,
                                          inString: termText,
                                          font: UIFont.systemFont(ofSize: 14),
                                          color: UIColor.black)
        
        termsLabel.attributedText = formattedText
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTermTapped))
        termsLabel.addGestureRecognizer(tap)
        termsLabel.isUserInteractionEnabled = true
    
    }
   private func loginUser() {
       guard let text = phoneNumberTextField.text else {
           print("Error: cannot crate textvariable")
           return
       }
       
       guard let url = URL(string: URLString.URL_LOGIN_USER) else {
        print("Error: cannot create URL")
        return
        }
       
       guard let phoneNumber = Int(text) else { print("Error: phone number"); return }
       
        // Add data to the model
        let uploadDataModel = LoginRequest(phone: phoneNumber)
        
        // Convert model to JSON data
        guard let jsonData = try? JSONEncoder().encode(uploadDataModel) else {
            print("Error: Trying to convert model to JSON data")
            return
        }
        
        // Create the url request
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type") // the request is JSON
        request.setValue("application/json", forHTTPHeaderField: "Accept") // the response expected to be in JSON format
        request.httpBody = jsonData
       createActivityIndicator()
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                DispatchQueue.main.async {
                    self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                    self.activityIndicator.stopAnimating()
                    self.progessView.removeFromSuperview()
                }
                print("Error: error calling POST")
                return
            }
            guard let data = data else {
                DispatchQueue.main.async {
                    self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                    self.activityIndicator.stopAnimating()
                    self.progessView.removeFromSuperview()
                }
                print("Error: Did not receive data")
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                DispatchQueue.main.async {
                    self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                    self.activityIndicator.stopAnimating()
                    self.progessView.removeFromSuperview()
                }
                print("Error: HTTP request failed")
                return
            }
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                    DispatchQueue.main.async {
                        self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                        self.activityIndicator.stopAnimating()
                        self.progessView.removeFromSuperview()
                    }
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                    print("Error: Cannot convert JSON object to Pretty JSON data")
                    DispatchQueue.main.async {
                        self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                        self.activityIndicator.stopAnimating()
                        self.progessView.removeFromSuperview()
                    }
                    return
                }
                guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                    DispatchQueue.main.async {
                        self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                        self.activityIndicator.stopAnimating()
                        self.progessView.removeFromSuperview()
                    }
                    print("Error: Couldn't print JSON in String")
                    return
                }
                print("jsonObject=\(jsonObject)")
                print("prettyJsonData=\(prettyJsonData)")
                print("prettyPrintedJson=\(prettyPrintedJson)")

                let decoder = JSONDecoder()
                let log = try decoder.decode(LoginResponse.self, from: prettyJsonData)
                if log.success {
                    DispatchQueue.main.async {
                        let signInConfirmationViewController: SignInConfirmationViewController = self.mainStoryboard.instantiateViewController(withIdentifier: "SignInConfirmationViewControllerId") as! SignInConfirmationViewController
                        signInConfirmationViewController.phoneNumber = self.phoneNumberTextField.text ?? ""
                        self.present(signInConfirmationViewController, animated: true, completion: nil)
                        self.activityIndicator.stopAnimating()
                        self.progessView.removeFromSuperview()
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        self.showToast(message: "NOT FOUND", font: .systemFont(ofSize: 12.0))
                        self.activityIndicator.stopAnimating()
                        self.progessView.removeFromSuperview()
                    }
                }
            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }.resume()
        
    }
}

protocol SignInViewControllerDelegate : AnyObject {
    func changeLanguage()
    func termsOfUse(_ bool : Bool)
}
extension SignInViewController : UITextFieldDelegate, SignInViewControllerDelegate {
    func termsOfUse(_ bool: Bool) {
        if bool {
            checkBoxButton.setImage(UIImage(systemName: "checkmark", withConfiguration: smallConfig), for: .normal)
            checkmarkVisibility = false
        } else {
            checkBoxButton.setImage(whiteImage, for: .normal)
            checkmarkVisibility = true
            checkBoxButton.scalesLargeContentImage = false


        }
    }
    
    func changeLanguage() {
        if let  language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        if currentLanguage == "tk" {
            changeLanguageButton.setTitle("Türkmençe     ", for: .normal)
            termText = "Ulanyş düzgünnamasyny kabul edýarin"
            term = "Ulanyş düzgünnamasyny"
            phoneNumberTextField.label.text = "Telefon belgisi"
            phoneNumberTextField.leadingAssistiveLabel.text = "+993-den galany ýaz"
           
        }
        if currentLanguage == "en" {
            changeLanguageButton.setTitle("English     ", for: .normal)
            termText = "I agree to the Terms of Use"
            term = "Terms of Use"
            phoneNumberTextField.label.text = "Phone number"
            phoneNumberTextField.leadingAssistiveLabel.text = "Write without +993"

        }
        if currentLanguage == "ru" {
            changeLanguageButton.setTitle("Pусский     ", for: .normal)
            termText = "Я принимаю Условия использования"
            term = "Условия использования"
            phoneNumberTextField.label.text = "Номер телефона"
            phoneNumberTextField.leadingAssistiveLabel.text = "Пишите без +993"
 
        }
        let formattedText = format(strings: [term],
                                          linkFont: UIFont.systemFont(ofSize: 14),
                                          linkColor: UIColor.link,
                                          inString: termText,
                                          font: UIFont.systemFont(ofSize: 14),
                                          color: UIColor.black)
        
        termsLabel.attributedText = formattedText
        
    }
    
    //MARK: UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phoneNumberTextField {
            phoneNumberTextField.editingStyle()
        }
        activeField = textField

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == phoneNumberTextField {
            phoneNumberTextField.defaultStyle()
        }
        activeField = nil

    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text{
            let invalidCharacters =
            CharacterSet ( charactersIn: "1234567890" ) .inverted
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            if  newString.count <= 8 {
                if textField == phoneNumberTextField {
                    phoneNumberTextField.editingStyle()
                    return ( string.rangeOfCharacter (from: invalidCharacters) == nil )
                    
                }
            }
        }
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        phoneNumberTextField.defaultStyle()
        return true
    }
}



//
//  AddCardViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 19.01.2022.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields


class NewCardViewController: UIViewController {

    
    let utils = Utils()
    var cards : [Card] = []
    var defaults = UserDefaults.standard
    weak var delegate : MyCardsViewControllerDelegate?
    weak var activeField : UITextField?

    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var cardNumberTextField: MDCOutlinedTextField!
    @IBOutlet weak var nameLastnameTextField: MDCOutlinedTextField!
    @IBOutlet weak var cardDateTextField: MDCOutlinedTextField!
    @IBOutlet weak var contentViewSafeArea: UIView!
    @IBOutlet weak var contentScrollView: UIView!
    @IBOutlet weak var help2Button: LocalizableButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
    
        cardNumberTextField.delegate = self
        nameLastnameTextField.delegate = self
        cardDateTextField.delegate = self
        
        customizingUI()
    
        utils.roundCorners(view: toolbarView, radius: 20)
        roundView.layer.cornerRadius = 20
        continueButton.layer.cornerRadius = 20
        
        // TODO: @Copy-pasta
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        view.addGestureRecognizer(tap)
   
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        cardNumberTextField.becomeFirstResponder()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }

    @IBAction func helpToolbarAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HelpViewControllerID") as! HelpViewController
        vc.id = "NewCardViewController"
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func helpAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HelpViewControllerID") as! HelpViewController
        vc.id = "NewCardViewController"
        present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func continueAction(_ sender: Any) {
        let one = utils.validateNumberRange(view: cardNumberTextField, min: 12, max: 12)
        let two = utils.validateNumberRange(view: nameLastnameTextField, min: 2, max: 60)
        let three = utils.validateNumberRange(view: cardDateTextField, min: 5, max: 5)
        let four = dateValidator()
        
        if one && two && three && four {
            //MARK: Save database
            guard let cardNumber = cardNumberTextField.text else {return}
            guard let nameLastname = nameLastnameTextField.text else {return}
            guard let cardDate = cardDateTextField.text else {return}
            
            
            if let data = defaults.data(forKey: "cards") {
                do {
                    let decoder = JSONDecoder()
                    let card_2 = try decoder.decode([Card].self, from: data)
                    cards = card_2
                } catch {
                    print(error)
                }
            }
            let card = Card(cardNumber: cardNumber, nameLastName: nameLastname, cardDate: cardDate, title: " \(cardNumber.substring(from: 8))")
            cards.append(card)
                   
            do {
                let encoder = JSONEncoder()
                let data = try encoder.encode(cards)
                defaults.set(data, forKey: "cards")
            } catch {
                print(error)
            }
            self.delegate?.reloadDataTableView()
            cardDateTextField.text = ""
            nameLastnameTextField.text = ""
            cardNumberTextField.text = ""
            
//            let language =  defaults.string(forKey: "CurrentLanguage")
            if utils.getLanguage() == "tk" {
                showToast(message: "Kart goşuldy", font: .systemFont(ofSize: 12.0))
            }
            if utils.getLanguage() == "en" {
                showToast(message: "Card added", font: .systemFont(ofSize: 12.0))
            }
            if utils.getLanguage() == "ru" {
                showToast(message: "Карта добавлена", font: .systemFont(ofSize: 12.0))
            }
         }
        
    }
    
    //keyboard
    @objc func keyboardDidShow(notification: Notification) {
        print("show keyboard")
        let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        guard let activeField = activeField, var keyboardHeight = keyboardSize?.height else { return }
           
        let tempKeyboardAdditional : CGFloat = 0.0
        let tempMoveAdditional : CGFloat = 0.0
           
        keyboardHeight += tempKeyboardAdditional  // TODO: Temporary hack. Find real value
        let moveViewFromBottomSize = keyboardHeight + tempMoveAdditional
           
           
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: moveViewFromBottomSize, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        let activeRect = activeField.convert(activeField.bounds, to: scrollView)
        scrollView.scrollRectToVisible(activeRect, animated: true)
    }

    @objc func keyboardWillBeHidden(notification: Notification) {
        print("Hide keyboard")
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
       
    @objc func DismissKeyboard(){
        view.endEditing(true)
    }

    private func dateValidator() -> Bool {
        guard let dateString = cardDateTextField.text else {return false}
        let s2 = dateString.prefix(2)
        guard let i = Int(s2) else {return false}
        if i > 12 {
            cardDateTextField.errorStyle()
            return false
        }
        cardDateTextField.defaultStyle()
        return true
    }
    
    private func funcFormat(with mask:String, phone: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression, range: nil)
        var result = ""
        var index = numbers.startIndex
        
        for ch in mask where index < numbers.endIndex{
            if ch == "X" {
                result.append(numbers[index])
                index = numbers.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    private func customizingUI() {
        if utils.getLanguage() == "tk" {
            cardNumberTextField.label.text = "Kart belgisi"
            cardNumberTextField.leadingAssistiveLabel.text = "16 san bolmaly"
            cardDateTextField.label.text = "Kartyň möhleti (Aý/Ýyl)"
            cardDateTextField.leadingAssistiveLabel.text = "Kartyň möhleti dogry ýaz"
            nameLastnameTextField.label.text = "Familiýasy Ady"
            nameLastnameTextField.leadingAssistiveLabel.text = "Kartyň ýüzündakileri ýazyň"
 
            
        } else if utils.getLanguage() == "en" {
            cardNumberTextField.label.text = "Card number"
            cardNumberTextField.leadingAssistiveLabel.text = "Enter 16 digits"
            cardDateTextField.label.text = "Card expiration (Month/Year)"
            cardDateTextField.leadingAssistiveLabel.text = "Enter correct expiration date"
            nameLastnameTextField.label.text = "Surname Name"
            nameLastnameTextField.leadingAssistiveLabel.text = "On the card front"
 
        } else if utils.getLanguage() == "ru" {
            cardNumberTextField.label.text = "Номер карты"
            cardNumberTextField.leadingAssistiveLabel.text = "Введите 16 цифр"
            cardDateTextField.label.text = "Срок окончания карты (Месяц/Год)"
            cardDateTextField.leadingAssistiveLabel.text = "Введите верный срок"
            nameLastnameTextField.label.text = "Фамилия Имя"
            nameLastnameTextField.leadingAssistiveLabel.text = "На лицевой стороне карты"
 
        }
        
        nameLastnameTextField.defaultStyle()
        cardDateTextField.defaultStyle()
        cardNumberTextField.defaultStyle()
        
        nameLastnameTextField.clearButtonMode = .whileEditing
        nameLastnameTextField.keyboardType = .default
        cardDateTextField.clearButtonMode = .whileEditing
        cardDateTextField.keyboardType = .numberPad
        cardNumberTextField.clearButtonMode = .whileEditing
        cardNumberTextField.keyboardType = .numberPad
       
        //Theme
        self.view.backgroundColor = Theme.backgroundColorLight
        self.toolbarView.backgroundColor = Theme.backgroundColorLight
        self.continueButton.setTitleColor(Theme.buttonTextColor, for: .normal)
        self.contentViewSafeArea.backgroundColor = Theme.backgroundColor
        self.contentScrollView.backgroundColor = Theme.backgroundColor
        self.roundView.backgroundColor = Theme.backgroundColorLight
        self.cardNumberTextField.backgroundColor = Theme.backgroundColorLight
        self.cardNumberTextField.textColor = Theme.textColor
        self.cardDateTextField.backgroundColor = Theme.backgroundColorLight
        self.cardDateTextField.textColor = Theme.textColor
        self.backButton.tintColor = Theme.textColor
        self.helpButton.tintColor = Theme.textColor
        //#3EA3EB
        self.help2Button.setTitleColor(UIColor(cgColor: CGColor(red: CGFloat(0x3E)/255, green: CGFloat(0xA3)/255, blue: CGFloat(0xEB)/255, alpha: 1)), for: .normal)
        self.nameLastnameTextField.textColor  = Theme.textColor
        self.nameLastnameTextField.backgroundColor = Theme.backgroundColorLight
        
        //shadow
        utils.setBottomShadow(toolbarView)
        utils.setShadow(roundView)
        
    }

}
extension NewCardViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == cardNumberTextField {
            cardNumberTextField.editingStyle()
        }
        if textField == cardDateTextField {
            cardDateTextField.editingStyle()
        }
        if textField == nameLastnameTextField {
            nameLastnameTextField.editingStyle()
        }
        activeField = textField

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == cardNumberTextField {
            cardNumberTextField.defaultStyle()
        }
        if textField == cardDateTextField {
            cardDateTextField.defaultStyle()
        }
        if textField == nameLastnameTextField {
            nameLastnameTextField.defaultStyle()
        }
        activeField = nil

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            let invalidCharactersNumbers =
                CharacterSet ( charactersIn: "1234567890" ) .inverted
            
            let invalidCharacters =
                CharacterSet ( charactersIn: "QWERTYUIOPASDFGHJKLZXCVBNMÜüqwertyuiopasdfghjklzxcvbnmçäöňşý .-," ) .inverted
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            
            if textField == cardNumberTextField {
                if  newString.count <= 12 {
                    cardNumberTextField.editingStyle()
             return ( string.rangeOfCharacter (from: invalidCharactersNumbers) == nil )
                    
                }
            }
            
                if textField == cardDateTextField {
                    if  newString.count <= 5 {
                        cardDateTextField.editingStyle()
                        textField.text = funcFormat(with: "XX/XX", phone: newString)

                    }
                }
               
            if textField == nameLastnameTextField {
                if  newString.count < 100 {
                    nameLastnameTextField.editingStyle()
              return ( string.rangeOfCharacter (from: invalidCharacters) == nil )
                    
                }
                
            }
            
        }
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        nameLastnameTextField.defaultStyle()
        cardDateTextField.defaultStyle()
        cardNumberTextField.defaultStyle()
        return true
    }
}

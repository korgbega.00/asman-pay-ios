//
//  PasscodeTimerViewController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 17.02.2022.
//

import UIKit

class PasscodeTimerViewController: UIViewController {

    var second = 10
    var timer = Timer()
    @IBOutlet weak var timerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Theme.backgroundColorLight
        timerLabel.textColor = .white
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerSelector), userInfo: nil, repeats: true)

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDark = UserDefaults.standard.bool(forKey: "theme")
        if !isDark {
           return UIStatusBarStyle.darkContent
        } else {
            return UIStatusBarStyle.lightContent
        }
    }
    
    @objc func timerSelector(){
        second -= 1
        timerLabel.text = "Gaty köp synansyk, \(second) sekuntdan son yene synansyñ."
        if second <= 0 {
            timer.invalidate()
            self.dismiss(animated: false, completion: nil)
        }
    }
    
}



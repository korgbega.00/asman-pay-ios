//
//  LocalizableStuff.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 22.02.2022.
//

import Foundation
import UIKit


class LocalizableLabel: UILabel {
    @IBInspectable var translationKey: String?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: AppNotification.changeLanguage, object: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateUI()
    }
    
    @objc func updateUI() {
        if let key = self.translationKey {
            self.text = key.localized
        } else {
            assertionFailure("Translation not set for \(self.text ?? "")")
        }
    }
}


class LocalizableButton: UIButton {
    @IBInspectable var translationKey: String?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: AppNotification.changeLanguage, object: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateUI()
    }
    
    @objc func updateUI() {
        if let key = self.translationKey {
            self.setTitle(key.localized, for: .normal)
        } else {
            assertionFailure("Translation not set for \(self.title(for: .normal) ?? "")")
        }
    }
}


class LocalizableTabBarItem : UITabBarItem {
    @IBInspectable var translationKey: String?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: AppNotification.changeLanguage, object: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateUI()
    }
    
    @objc func updateUI() {
        if let key = self.translationKey {
            self.title = key.localized
        } else {
            assertionFailure("Translation not set for \(self.title)")
        }
    }
}


class LocalizableTextField : UITextField {
    @IBInspectable var translationKey: String?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: AppNotification.changeLanguage, object: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateUI()
    }
    
    @objc func updateUI() {
        if let key = self.translationKey {
            self.placeholder = key.localized
        } else {
            assertionFailure("Translation not set for \(self.placeholder)")
        }
    }
}









class LocalizableNavigationItem : UINavigationItem {
    @IBInspectable var translationKey: String?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: AppNotification.changeLanguage, object: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateUI()
    }
    
    @objc func updateUI() {
        if let key = self.translationKey {
            self.title = key.localized
        } else {
            assertionFailure("Translation not set for \(self.title)")
        }
    }
}


class LocalizableBarItem : UIBarButtonItem {
    @IBInspectable var translationKey: String?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: AppNotification.changeLanguage, object: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateUI()
    }
    
    @objc func updateUI() {
        if let key = self.translationKey {
            self.title = key.localized
        } else {
            assertionFailure("Translation not set for \(self.title)")
        }
    }
}

class LocalizableTextView : UITextView {
    @IBInspectable var translationKey: String?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: AppNotification.changeLanguage, object: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateUI()
    }
    
    @objc func updateUI() {
        if let key = self.translationKey {
            self.text = key.localized
        } else {
            assertionFailure("Translation not set for \(self.text)")
        }
    }
}

//
//  StringExtension.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 22.02.2022.
//

import Foundation
let LANGUAGE_KEY = "CurrentLanguage"

struct AppNotification {
    static let changeLanguage = Notification.Name("changelangauge")
}

extension String {
    var localized: String {
        // TODO: this function is just a prototype with misssing error checks. Add them!
        let defaults = UserDefaults.standard
        
        var currentLanguage = "ru"
        if let language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
        }
        
        let path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        let string = bundle?.localizedString(forKey: self, value: nil, table: nil)
        
        

        return string!
    }
    
    func localized(_ arguments: CVarArg...) -> String {
        return String(format: self.localized, arguments: arguments)
    }
}

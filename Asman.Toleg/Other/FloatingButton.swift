//
//  FloatingButton.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 25.01.2022.
//

import UIKit
import MaterialComponents.MaterialButtons

class FloatingButton: MDCFloatingButton {

    
    override func draw(_ rect: CGRect) {
        self.backgroundColor = .green
        self.tintColor = .white
        self.setImage(UIImage(systemName: "message.fill"), for: .normal)
    }
   
}

//
//  Styles.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 11.03.2022.
//

import Foundation
import UIKit
import MaterialComponents.MaterialButtons


struct Theme {

    static var backgroundColor : UIColor?
    static var backgroundColorLight : UIColor?
    static var textColor : UIColor?
    static var buttonTextColor : UIColor?
    static var iconTintColor : UIColor?
    static var iconTintColorDark : UIColor?
    static var lineColor : UIColor?
    static var tabBarTintColor : UIColor?
    static var buttonTapColor : UIColor?
    static var defaultSelectedColor : UIColor?
    static var shadowColorView : UIColor?
    static var shadowTabBar : CGColor?

    static public func defaultTheme() {
        //#F8F8F8
        self.backgroundColor = UIColor(cgColor: CGColor(red: CGFloat(0xF8)/255, green: CGFloat(0xF8)/255, blue: CGFloat(0xF8)/255, alpha: 1))
        //#FFFFFF
        self.backgroundColorLight = .white
        //#252525
        self.textColor = UIColor(cgColor: CGColor(red: CGFloat(0x25)/255, green: CGFloat(0x25)/255, blue: CGFloat(0x25)/255, alpha: 1))
        //#FFFFFF
        self.buttonTextColor = .white
        //#252525
        self.iconTintColor = UIColor(cgColor: CGColor(red: CGFloat(0x25)/255, green: CGFloat(0x25)/255, blue: CGFloat(0x25)/255, alpha: 1))
        //#252525
        self.iconTintColorDark = UIColor(cgColor: CGColor(red: CGFloat(0x25)/255, green: CGFloat(0x25)/255, blue: CGFloat(0x25)/255, alpha: 1))
        //#D8D8D8
        self.lineColor = UIColor(cgColor: CGColor(red: CGFloat(0xD8)/255, green: CGFloat(0xD8)/255, blue: CGFloat(0xD8)/255, alpha: 1))
        //#AEB1B5
        self.tabBarTintColor = UIColor(cgColor: CGColor(red: CGFloat(0x75)/255, green: CGFloat(0x75)/255, blue: CGFloat(0x75)/255, alpha: 1))
        //#EF1FB
        self.buttonTapColor = UIColor(cgColor: CGColor(red: CGFloat(0xE3)/255, green: CGFloat(0xF1)/255, blue: CGFloat(0xFB)/255, alpha: 1))
        //#F0F0F0
        self.defaultSelectedColor = UIColor(cgColor: CGColor(red: CGFloat(0xF0)/255, green: CGFloat(0xF0)/255, blue: CGFloat(0xF0)/255, alpha: 1))
        
        self.shadowColorView = .black
        //EFEFEF
        self.shadowTabBar = CGColor(red: CGFloat(0xEF)/255, green: CGFloat(0xEF)/255, blue: CGFloat(0xEF)/255, alpha: 1)
        
        updateDisplay()
    }

    static public func darkTheme() {
        //#17191B
        self.backgroundColor = UIColor(cgColor: CGColor(red: CGFloat(0x17)/255, green: CGFloat(0x19)/255, blue: CGFloat(0x1B)/255, alpha: 1))
        //#26282C
        self.backgroundColorLight = UIColor(cgColor: CGColor(red: CGFloat(0x26)/255, green: CGFloat(0x28)/255, blue: CGFloat(0x2C)/255, alpha: 1))
        //#DEDEDE
        self.textColor = UIColor(cgColor: CGColor(red: CGFloat(0xDE)/255, green: CGFloat(0xDE)/255, blue: CGFloat(0xDE)/255, alpha: 1))
        //#26282C
        self.buttonTextColor = UIColor(cgColor: CGColor(red: CGFloat(0x26)/255, green: CGFloat(0x28)/255, blue: CGFloat(0x2C)/255, alpha: 1))
        //#DEDEDE
        self.iconTintColor =  UIColor(cgColor: CGColor(red: CGFloat(0xDE)/255, green: CGFloat(0xDE)/255, blue: CGFloat(0xDE)/255, alpha: 1))
        //#7F8696
        self.iconTintColorDark = UIColor(cgColor: CGColor(red: CGFloat(0x7F)/255, green: CGFloat(0x86)/255, blue: CGFloat(0x96)/255, alpha: 1))
        //#606675
        self.lineColor = UIColor(cgColor: CGColor(red: CGFloat(0x60)/255, green: CGFloat(0x66)/255, blue: CGFloat(0x75)/255, alpha: 1))
        //#606675
        self.tabBarTintColor = UIColor(cgColor: CGColor(red: CGFloat(0x60)/255, green: CGFloat(0x66)/255, blue: CGFloat(0x75)/255, alpha: 1))
        //#253542
        self.buttonTapColor = UIColor(cgColor: CGColor(red: CGFloat(0x25)/255, green: CGFloat(0x35)/255, blue: CGFloat(0x42)/255, alpha: 1))
        //#393B3E
        self.defaultSelectedColor = UIColor(cgColor: CGColor(red: CGFloat(0x39)/255, green: CGFloat(0x3B)/255, blue: CGFloat(0x3E)/255, alpha: 1))

        //#16181A
        self.shadowColorView = UIColor(cgColor: CGColor(red: CGFloat(0x16)/255, green: CGFloat(0x18)/255, blue: CGFloat(0x1A)/255, alpha: 1))
        //#17191B
        self.shadowTabBar = CGColor(red: CGFloat(0x17)/255, green: CGFloat(0x19)/255, blue: CGFloat(0x1B)/255, alpha: 1)
        updateDisplay()
    }

    static public func updateDisplay() {
        //MARK: proxy button
        let proxyButton = UIButton.appearance()
        proxyButton.setTitleColor(Theme.textColor, for: .normal)
        proxyButton.imageView?.tintColor = iconTintColor
        
        //MARK: proxy view
        let proxyView = UIView.appearance()
//        proxyView.backgroundColor = backgroundColorLight
        
        
        //MARK: proxy label
        let proxyLabel = UILabel.appearance()
        proxyLabel.textColor = textColor
        
//        //MARK: proxyTextField
//        let proxyTextField = UITextField.appearance()
//        proxyTextField.textColor = self.textColor
//        proxyTextField.tintColor = textColor
        
//        //MARK: proxy imageView
//        let proxyImageView = UIImageView.appearance()
//        proxyImageView.tintColor = iconTintColor
        
        
    }
}

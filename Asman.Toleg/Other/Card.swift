//
//  MyCardDatabase.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 27.01.2022.
//

import Foundation

struct Card: Codable {
    
    let cardNumber: String
    let nameLastName : String
    let cardDate : String
    let title : String
}

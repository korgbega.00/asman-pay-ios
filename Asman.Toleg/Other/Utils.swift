//
//  Utils.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 12.01.2022.
//

import Foundation
import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields



class Utils {
    
    public func getLanguage() -> String {
        let LANGUAGE_KEY = "CurrentLanguage"
        let defaults = UserDefaults.standard
        var currentLanguage = "ru"
        if let language = defaults.string(forKey: LANGUAGE_KEY) {
            currentLanguage = language
            return currentLanguage
        }
   
        return "ru"
    }
    
    // Returns the most recently presented UIViewController (visible)
    
    func getCurrentViewController() -> UIViewController? {

//        // If the root view is a navigation controller, we can just return the visible ViewController
//        if let navigationController = getNavigationController() {
//
//            return navigationController.visibleViewController
//        }

        // Otherwise, we must get the root UIViewController and iterate through presented views
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {

            var currentController: UIViewController! = rootController

            // Each ViewController keeps track of the view it has presented, so we
            // can move from the head to the tail, which will always be the current view
            while( currentController.presentedViewController != nil ) {

                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
    }

//    // Returns the navigation controller if it exists
//    class func getNavigationController() -> UINavigationController? {
//
//        if let navigationController = UIApplication.shared.keyWindow?.rootViewController  {
//
//            return navigationController as? UINavigationController
//        }
//        return nil
//    }

    public func validateNumberRange(view: MDCOutlinedTextField, min: Int, max: Int) -> Bool{
        guard let count = view.text?.count else {
            view.errorStyle()
            return false
        }
        
        if (count >= min) && (count <= max) {
                return true
            }
        
        view.errorStyle()
        return false
    }
    public func indicatorIsVisibility(indicator : UIActivityIndicatorView, view : UIView , bool: Bool) {
        if bool {
        indicator.alpha = 1.0
        view.alpha = 0.0
        
    } else {
        indicator.alpha = 0.0
        view.alpha = 1.0
        }
    }
    
    public func roundCorners(view : UIView, radius : Int) {
        view.clipsToBounds = false
        view.layer.cornerRadius = CGFloat(radius)
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]

    }
    
    public func setShadow(_ view: UIView){
        let color = Theme.shadowColorView
        view.layer.shadowColor = color?.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowRadius = 5
     
   }
    public func setBottomShadow(_ view: UIView){
        let color = Theme.shadowColorView
        view.layer.shadowColor = color?.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowRadius = 5
        view.layer.shadowPath = UIBezierPath(roundedRect: CGRect(x: 0, y: view.bounds.maxY - view.layer.shadowRadius, width: view.bounds.width, height: view.layer.shadowRadius), cornerRadius: view.layer.cornerRadius).cgPath
     
   }
}

extension UIViewController {
    
func showToast(message : String, font: UIFont) {
    let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
    toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    toastLabel.textColor = UIColor.white
    toastLabel.font = font
    toastLabel.textAlignment = .center;
    toastLabel.text = message
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    self.view.addSubview(toastLabel)
    UIView.animate(withDuration: 1.0, delay: 2.0, options: .curveEaseOut, animations: {
         toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
}
    func showMenu(point : CGPoint, contentView : UIView, title : String) -> UIButton {
        let menuButton = UIButton()
        menuButton.frame = CGRect(x: point.x, y: point.y, width: contentView.frame.width/2, height: 40)
        menuButton.setTitle(title, for: .normal)
        menuButton.setTitleColor(Theme.textColor, for: .normal)
        menuButton.titleLabel?.font = .systemFont(ofSize: 12.0)
        menuButton.backgroundColor = Theme.backgroundColorLight
        menuButton.setImage(UIImage(systemName: "trash"), for: .normal)
        menuButton.layer.cornerRadius = 5
        //#C7C7C7
        menuButton.layer.shadowColor = CGColor(red: CGFloat(0xC7)/255 , green: CGFloat(0xC7)/255, blue: CGFloat(0xC7)/255, alpha: 1)
        menuButton.contentHorizontalAlignment = .center
        menuButton.tintColor = Theme.textColor
        menuButton.layer.shadowOpacity = 0.75
        menuButton.layer.shadowOffset = .zero
        menuButton.layer.shadowRadius = 5
        menuButton.layer.shadowPath = UIBezierPath(roundedRect: menuButton.bounds, cornerRadius: menuButton.layer.cornerRadius).cgPath
        
        return menuButton
    }
 }
extension MDCOutlinedTextField {
    func errorStyle(){
        let imageView = UIImageView(image: UIImage(named: "error"))
        imageView.frame.size = CGSize(width: 25, height: 25)
        self.trailingView = imageView
        self.trailingViewMode = .always
        self.leadingAssistiveLabel.alpha = 1

        //colors
        let color = UIColor(cgColor: CGColor(red: 1, green: 0, blue: 0, alpha: 1))
        self.setOutlineColor(color, for: .normal)
        self.setOutlineColor(color, for: .disabled)
        self.setOutlineColor(color, for: .editing)
        self.setLeadingAssistiveLabelColor(color, for: .normal)
        self.setLeadingAssistiveLabelColor(color, for: .disabled)
        self.setLeadingAssistiveLabelColor(color, for: .editing)
        self.tintColor = color
        self.setNormalLabelColor(color, for: .normal)
        self.setNormalLabelColor(color, for: .disabled)
        self.setNormalLabelColor(color, for: .editing)
        
        self.setFloatingLabelColor(color, for: .normal)
        self.setFloatingLabelColor(color, for: .disabled)
        self.setFloatingLabelColor(color, for: .editing)
    
    }
    func editingStyle(){
        self.trailingViewMode = .never
        self.leadingAssistiveLabel.alpha = 0


        //colors
        //3EA3EB
        let color = UIColor(cgColor: CGColor(red: CGFloat(0x3E)/255, green: (0xA3)/255, blue: (0xEB)/255, alpha: 1.0))
        let textColor =  UIColor(cgColor: CGColor(red: CGFloat(0x25)/255, green: CGFloat(0x25)/255, blue: CGFloat(0x25)/255, alpha: 1))
        
        self.setOutlineColor(color, for: .normal)
        self.setOutlineColor(color, for: .disabled)
        self.setOutlineColor(color, for: .editing)
        self.setLeadingAssistiveLabelColor(color, for: .normal)
        self.setLeadingAssistiveLabelColor(color, for: .disabled)
        self.setLeadingAssistiveLabelColor(color, for: .editing)
        self.tintColor = color
        self.setNormalLabelColor(color, for: .normal)
        self.setNormalLabelColor(color, for: .disabled)
        self.setNormalLabelColor(color, for: .editing)
        
        self.setFloatingLabelColor(color, for: .normal)
        self.setFloatingLabelColor(color, for: .disabled)
        self.setFloatingLabelColor(color, for: .editing)
        
        self.setTextColor(Theme.textColor ?? textColor, for: .normal)
        self.setTextColor(Theme.textColor ?? textColor, for: .disabled)
        self.setTextColor(Theme.textColor ?? textColor, for: .editing)

  
    }
    func defaultStyle(){
        self.trailingViewMode = .never
        self.leadingAssistiveLabel.alpha = 0

        //colors
        //C8CACB
        let color = UIColor(cgColor: CGColor(red: CGFloat(0xC8)/255, green: (0xCA)/255, blue: (0xCB)/255, alpha: 1.0))
        //252525
        let textColor =  UIColor(cgColor: CGColor(red: CGFloat(0x25)/255, green: CGFloat(0x25)/255, blue: CGFloat(0x25)/255, alpha: 1))
        
        self.setOutlineColor(color, for: .normal)
        self.setOutlineColor(color, for: .disabled)
        self.setOutlineColor(color, for: .editing)
        self.setLeadingAssistiveLabelColor(color, for: .normal)
        self.setLeadingAssistiveLabelColor(color, for: .disabled)
        self.setLeadingAssistiveLabelColor(color, for: .editing)
        self.tintColor = color
        self.setNormalLabelColor(Theme.textColor ?? textColor, for: .normal)
        self.setNormalLabelColor(Theme.textColor ?? textColor, for: .disabled)
        self.setNormalLabelColor(Theme.textColor ?? textColor, for: .editing)
        
        self.setFloatingLabelColor(Theme.textColor ?? .black, for: .normal)
        self.setFloatingLabelColor(Theme.textColor ?? .black, for: .disabled)
        self.setFloatingLabelColor(Theme.textColor ?? .black, for: .editing)
    }
}

extension UITapGestureRecognizer {
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        guard let attrString = label.attributedText else {
            return false
        }

        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: .zero)
        let textStorage = NSTextStorage(attributedString: attrString)

        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        textContainer.lineFragmentPadding = 0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}
extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }

    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }

    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }

    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
}




//extension UITextField {
//    func setRightIcon(_ icon: UIImage){
//        let padding = 15
//        let width = 25
//        let height = 15
//        let outerView = UIView(frame: CGRect(x: -padding, y: 0, width: width+padding, height: height))
//        let iconView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
//        iconView.image = icon
//        iconView.contentMode = .scaleAspectFill
//        outerView.addSubview(iconView)
//        rightView = outerView
//        rightViewMode = .always
//    }
//}
//
//

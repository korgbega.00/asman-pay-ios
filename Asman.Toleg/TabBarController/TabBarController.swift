//
//  TabBarController.swift
//  Asman.Toleg
//
//  Created by Shatlyk on 07.01.2022.
//

import UIKit
//import PTCardTabBar

class TabBarController: UITabBarController {
    
    let utils = Utils()
    override func viewDidLoad() {
        super.viewDidLoad()
    
        tabBar.barTintColor = Theme.backgroundColorLight
        roundCorners(view: tabBar, radius: 20)
        self.view.backgroundColor = Theme.backgroundColor
        utils.setShadow(tabBar)
  
        tabBar.layer.borderWidth = 1
        tabBar.layer.borderColor = Theme.shadowTabBar
      }
    
    private func roundCorners(view : UIView, radius : Int) {
        view.clipsToBounds = true
        view.layer.cornerRadius = CGFloat(radius)
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
    }
    
}
